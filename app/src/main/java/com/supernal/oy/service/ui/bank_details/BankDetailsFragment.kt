package com.supernal.oy.service.ui.bank_details

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.supernal.oy.databinding.FragmentBankDetailsBinding

class BankDetailsFragment : Fragment() {

    private lateinit var bankDetailsViewModel: BankDetailsViewModel
    private var _binding: FragmentBankDetailsBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bankDetailsViewModel =
        ViewModelProvider(this).get(BankDetailsViewModel::class.java)
        _binding = FragmentBankDetailsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        return root
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}