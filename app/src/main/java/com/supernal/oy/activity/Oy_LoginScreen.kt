package com.supernal.oy.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.facebook.*
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.supernal.oy.BuildConfig
import com.supernal.oy.R
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.concurrent.TimeUnit

class Oy_LoginScreen : Activity() {
    lateinit var otpbtn: Button
    private val RC_SIGN_IN = 123
    lateinit var callbackManager: CallbackManager
    lateinit var auth: FirebaseAuth
    lateinit var mobile_number: EditText
    lateinit var facebook_login: LoginButton
    lateinit var googleSignInButton: SignInButton
    lateinit var storedVerificationId: String
    lateinit var sharedPreferences: SharedPreferences
    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private val TAG = "Oy_LoginScreen"
    lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var preferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_loginscreen)


        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        servicesharedPreferences = getSharedPreferences("serviceloginprefs", Context.MODE_PRIVATE)

        FacebookSdk.sdkInitialize(this.applicationContext)

        if (BuildConfig.DEBUG) {
            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        }
        callbackManager = CallbackManager.Factory.create()
        facebook_login = findViewById(R.id.facebook_btn)
        otpbtn = findViewById(R.id.confirm_btn)
        googleSignInButton = findViewById(R.id.google_btn)
        mobile_number = findViewById(R.id.mobileEditTxt)

        auth = FirebaseAuth.getInstance()

        facebook_login.setReadPermissions("email", "public_profile")

        val permissions: MutableList<String> = ArrayList()
        permissions.add("email")
        printHashKey()
        facebook_login.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult) {
                handleFacebookAccessToken(result.accessToken)
                Log.e("accessToken", "" + result.accessToken)



                }

            override fun onCancel() {
                Toast.makeText(
                    baseContext, "Authentication failed.",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onError(error: FacebookException) {
                Toast.makeText(
                    baseContext, "Authentication.",
                    Toast.LENGTH_SHORT
                ).show()

                Log.e("err", "" + error.message)
            }
        })
        createRequest()
        var currentUser = auth.currentUser
        preferences = getSharedPreferences("selection", Context.MODE_PRIVATE)
        otpbtn.setOnClickListener {


            otpLogin()

        }
        googleSignInButton.setOnClickListener {
            googleSignIn()
        }

        //phone number authentication
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                startActivity(Intent(applicationContext, Oy_SuccessScreen::class.java))
                finish()
            }

            override fun onVerificationFailed(p0: FirebaseException) {
                Toast.makeText(applicationContext, "Failed", Toast.LENGTH_LONG).show()
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                Log.d("TAG", "onCodeSent:$verificationId")
                storedVerificationId = verificationId
                resendToken = token
                var intent = Intent(applicationContext, Oy_OtpScreen::class.java)
                intent.putExtra("storedVerificationId", storedVerificationId)
                startActivity(intent)
                finish()
            }
        }

        val editor = sharedPreferences.edit().clear().commit()
        val editors = servicesharedPreferences.edit().clear().commit()
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.e("token", "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.e("ss", "signInWithCredential:success")

                    Toast.makeText(
                        baseContext, "Authentication sucess.",
                        Toast.LENGTH_SHORT
                    ).show()
                    val user = auth.currentUser
                } else {
                    // If sign in fails, display a message to the user.
                    Log.e("scd", "signInWithCredential:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    private fun printHashKey() {
        try {
            val info =
                packageManager.getPackageInfo("com.supernal.oy", PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA1")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Log.e("KeyHash:", e.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("KeyHash:", e.toString())
        }
    }


    // gmail signin authentication
    private fun createRequest() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id1))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    // gmail login

    private fun googleSignIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase

                val account = task.getResult(ApiException::class.java)!!
                Toast.makeText(this, "firebaseAuthWithGoogle:", Toast.LENGTH_SHORT).show()
                Log.e(TAG, "firebaseAuthWithGoogle:" + account.email)

                val email = account.email.toString()

                Log.e("checkemail", "" + email)

                val editor = sharedPreferences.edit()
                editor.putString("useremail", email)
                editor.commit()
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(
                    this,
                    "Failed to sign in, Error: " + e.getStatusCode(),
                    Toast.LENGTH_SHORT
                ).show();
                Log.w(TAG, "Google sign in failed", e)
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data) //facebook  callback
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Toast.makeText(this, "signInWithCredential:success", Toast.LENGTH_SHORT).show()
                    Log.d(TAG, "signInWithCredential:success")
                    var currentUser = auth.currentUser
                    startActivity(Intent(applicationContext, Oy_SuccessScreen::class.java))
                } else {
                    // If sign in fails, display a message to the user.
                    Toast.makeText(this, "signInWithCredential:failure", Toast.LENGTH_SHORT).show()
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                }
            }
    }


//otp login

    private fun otpLogin() {
        var number = mobile_number.text.toString().trim()

        val editor = sharedPreferences.edit()
        editor.putString("usermobilenumber", number)
        Log.e("number", "" + number)
        editor.commit()

        if (!number.isEmpty()) {
            number = "+91" + number
            sendVerificationCode(number)
        } else {
            Toast.makeText(this, "Enter mobile number", Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendVerificationCode(number: String) {
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(number)
            .setTimeout(60L, TimeUnit.SECONDS)
            .setCallbacks(callbacks)
            .setActivity(this)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }
}