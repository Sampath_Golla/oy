package com.supernal.oy.adapters

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.models.LeadsDefaultListResponse


class Leads_adapter(var context: Context, var list: List<LeadsDefaultListResponse>) :
    RecyclerView.Adapter<Leads_adapter.MyViewHolder>() {

    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var category_ist: List<LeadsDefaultListResponse>

    //inflating and returning our view holder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        servicesharedPreferences = context.getSharedPreferences("serviceloginprefs", Context.MODE_PRIVATE)

        val view = LayoutInflater.from(context)
            .inflate(R.layout.leads_adapter, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {


        return list.size


    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.provider_name.text = list.get(position).first_name
        holder.location.text = list.get(position).address
        holder.work_desc.text = list.get(position).job_description
        holder.time_desc.text =
            list.get(position).date_scheduled + "   "+ list.get(position).time_scheduled

        var categoryimage = list.get(position).job_image

        Glide.with(context).load(categoryimage)
            .apply(RequestOptions().fitCenter())
            .error(R.drawable.electrician)
            .into(holder.serviceimage)
    }


    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {


        val serviceimage = itemView!!.findViewById(R.id.serviceimage) as ImageView
        val provider_name = itemView!!.findViewById(R.id.provider_name) as TextView
        val location = itemView!!.findViewById(R.id.location) as TextView
        val work_desc = itemView!!.findViewById(R.id.work_desc) as TextView
        val time_desc = itemView!!.findViewById(R.id.time_desc) as TextView


    }
}