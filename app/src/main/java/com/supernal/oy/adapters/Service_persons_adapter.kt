package com.supernal.oy.adapters

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.models.CategoryServicePersonsListResponse


class Service_persons_adapter(
    var context: Context,
    var list: List<CategoryServicePersonsListResponse>

) :
    RecyclerView.Adapter<Service_persons_adapter.MyViewHolder>() {
    var personslist: MutableList<String> = ArrayList()
    var serviceidlist: MutableList<String> = ArrayList()

    lateinit var sharedPreferences: SharedPreferences
    lateinit var category_ist: List<CategoryServicePersonsListResponse>

    //inflating and returning our view holder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        sharedPreferences = context.getSharedPreferences("loginprefs", Context.MODE_PRIVATE)

        val view = LayoutInflater.from(context)
            .inflate(R.layout.service_person_adapter, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {


        return list.size


    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.personname.text = list.get(position).fullname
        holder.servicerating.text = list.get(position).avg_stars


        var categoryimage = list.get(position).profile_pic

        Glide.with(context).load(categoryimage)
            .apply(RequestOptions().fitCenter())
            .error(R.drawable.electrician)
            .into(holder.profileimage)




        holder.servicecheckbox.setOnCheckedChangeListener { buttonView, isChecked ->


            if (isChecked) {

                serviceidlist.add(list.get(position).id.toString())
                var servicedpersonslist =LinkedHashSet(serviceidlist).toMutableList()


                Log.e("servicedpersonslistejen", "" + servicedpersonslist)

            }
        }


    }


    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {


        val profileimage = itemView!!.findViewById(R.id.profilepic) as ImageView
        val personname = itemView!!.findViewById(R.id.name) as TextView
        val profession = itemView!!.findViewById(R.id.profession) as TextView
        val servicerating = itemView!!.findViewById(R.id.servicerating) as TextView
        val servicecheckbox = itemView!!.findViewById(R.id.checkbox) as CheckBox


    }
}