package com.supernal.oy.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.supernal.oy.R

class Oy_OtpScreen:Activity() {
    lateinit var navigation:ImageView
    lateinit var auth: FirebaseAuth
    lateinit var otpgiven: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_otpscreen)
        auth= FirebaseAuth.getInstance()
        navigation=findViewById(R.id.navigation_button)
        otpgiven = findViewById(R.id.id_otp)
        val storedVerificationId = intent.getStringExtra("storedVerificationId")
        navigation.setOnClickListener{
            var otp = otpgiven.text.toString().trim()
            if (!otp.isEmpty()) {
                val credential: PhoneAuthCredential = PhoneAuthProvider.getCredential(
                    storedVerificationId.toString(), otp
                )
                signInWithPhoneAuthCredentials(credential)
            } else {
                Toast.makeText(this, "enter Otp", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun signInWithPhoneAuthCredentials(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this){task->
                if(task.isSuccessful){
                    startActivity(Intent(this, Oy_SuccessScreen::class.java))
                    finish()
                }else{
                    if(task.exception is FirebaseAuthInvalidCredentialsException){
                        Toast.makeText(this,"Invalid OTP",Toast.LENGTH_SHORT).show()
                    }
                }

            }
    }
}