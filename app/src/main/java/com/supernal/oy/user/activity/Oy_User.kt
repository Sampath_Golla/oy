package com.supernal.oy.user.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.supernal.oy.R
import com.supernal.oy.activity.Oy_LoginScreen
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.ActivityOyUserBinding
import com.supernal.oy.models.ProfileDefaultResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Oy_User : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityOyUserBinding
    lateinit var auth: FirebaseAuth
    lateinit var profile_name: TextView
    lateinit var profileImage: ImageView
    lateinit var navView: NavigationView

    lateinit var sharedPreferences: SharedPreferences
    lateinit var token: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOyUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.customView = binding.root
        setSupportActionBar(binding.appBarOyUser.userToolbar)

//        binding.appBarOyProvider.fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }

        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        auth = FirebaseAuth.getInstance()
        val drawerLayout: DrawerLayout = binding.drawerLayoutUser
        navView = binding.navUserView
        val navController = findNavController(R.id.nav_host_fragment_content_oy_user)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_user_home,
                R.id.nav_user_notifications,
                R.id.nav_user_profile,
                R.id.nav_user_bidStatus,
                R.id.nav_user_settings,
                R.id.nav_user_logout
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val headerView: View = binding.navUserView.getHeaderView(0)


        profile_name = headerView.findViewById(R.id.username)
        profileImage = headerView.findViewById(R.id.userImage)
        getusersProfile()



    }


    private fun getusersProfile() {


        try {

            if (NetWorkConection.isNEtworkConnected(this)) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)

                token = sharedPreferences.getString("token", "")!!
                Log.e("ptoken", "" + token)

                val call = apiServices.getuserprofile("Token $token")

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<ProfileDefaultResponse> {
                    override fun onResponse(
                        call: Call<ProfileDefaultResponse>,
                        response: Response<ProfileDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


                            val pfname: String? =
                                response.body()!!.data.first_name + response.body()!!.data.last_name
                            val pflast: String? = response.body()!!.data.last_name


                            val pimage: String? = response.body()!!.data.profile_pic

                            profile_name.text = "$pfname $pflast"

                            Glide.with(applicationContext).load(pimage)
                                .apply(RequestOptions().centerCrop())
                                .error(R.drawable.electrician)
                                .into(profileImage)

                        } else {
                            Toast.makeText(this@Oy_User, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<ProfileDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.oy_, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_oy_user)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {

        menuItem.isChecked = true

        val id = menuItem.itemId


        if (id == R.id.nav_user_home) {

        } else if (id == R.id.nav_user_notifications) {

        } else if (id == R.id.nav_user_profile) {

        } else if (id == R.id.nav_user_bidStatus) {

        } else if (id == R.id.nav_user_settings) {


        } else if (id == R.id.nav_user_logout) {

            Log.e("hai","hello checklogout")
            logoutfun()

        }

        val drawer = binding.drawerLayoutUser
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

     fun logoutfun() {

        val editor = sharedPreferences.edit().clear().commit()

        intent = Intent(applicationContext, Oy_LoginScreen::class.java)
        startActivity(intent)
        finish()

    }


}