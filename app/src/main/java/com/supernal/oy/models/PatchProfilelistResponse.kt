package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class PatchProfilelistResponse(
    @SerializedName("id") val id : Int,
    @SerializedName("first_name") val first_name : String,
    @SerializedName("last_name") val last_name : String,
    @SerializedName("mobile") val mobile : String,
    @SerializedName("email") val email : String,
    @SerializedName("profile_pic") val profile_pic : String,
    @SerializedName("gender") val gender : String,
    @SerializedName("latitude") val latitude : Double,
    @SerializedName("longitude") val longitude : Double,
    @SerializedName("address") val address : String

)