package com.supernal.oy.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.supernal.oy.R
import com.supernal.oy.service.ui.home.HomeFragment

class services_adapter(
    private val context: HomeFragment,
    private val numbersInWords: Array<String>,
    private val numberImage: IntArray
) : BaseAdapter() {

    override fun getCount(): Int {
        return numbersInWords.size
    }

    override fun getItem(p0: Int): Any? {
        return null
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertview: View?, parent: ViewGroup?): View? {
        var convertView = convertview
        var holder:ViewHolder
//        if (layoutInflater == null) {
//            val layoutInflater =
//                context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        }

        if(convertView==null){
            val mInflater = (context).layoutInflater
            convertView = mInflater.inflate(R.layout.oy_provider_home_adapter, parent, false)
        }
        holder=ViewHolder()

        holder.service_img=convertView!!.findViewById(R.id.service_img) as ImageView
        holder.service_txt=convertView!!.findViewById(R.id.service_txt) as TextView
        holder.service_img!!.setImageResource(numberImage[position])
        holder.service_txt!!.text=numbersInWords[position]
        return convertView
    }
    class ViewHolder {

        var service_img: ImageView?=null
        var service_txt: TextView?=null

    }
}