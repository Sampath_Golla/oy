package com.supernal.oy.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.supernal.oy.R
import java.io.IOException
import java.util.*

class Oy_SuccessScreen : Activity() {
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var nav_btn: ImageView
    private val permissionCode = 101
    lateinit var sharedPreferences: SharedPreferences
    lateinit var currentLocation: Location
    var lng: Double = 0.0
    var lat: Double = 0.0
    lateinit var Address: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_success_screen)
        nav_btn = findViewById(R.id.nav_btn)

        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fetchLocation()
        nav_btn.setOnClickListener {
            startActivity(Intent(this@Oy_SuccessScreen, Oy_SelectionScreen::class.java))
            finish()
        }
    }

    companion object

    fun fetchLocation() {
        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                permissionCode
            )
            return
        }
        val task = fusedLocationProviderClient.lastLocation
        task.addOnSuccessListener { location ->
            if (location != null) {
                currentLocation = location
                lat = currentLocation.latitude.toDouble()
                lng = currentLocation.longitude.toDouble()
                val address = getAddress(lat, lng)

            }
        }
    }

    private fun getAddress(lat: Double, lng: Double): Any {
        val result = StringBuilder()
        val geocoder = Geocoder(this, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(lat, lng, 1)
            if (addresses.size > 0) {
                val address = addresses[0]
                result.append(address.subThoroughfare).append(",")
                result.append(address.thoroughfare).append(",")
                result.append(address.subLocality).append(",")
                result.append(address.locality).append(",")
                result.append(address.countryName)
                Address = result.toString()
                val editor=sharedPreferences.edit()
                editor.putString("location",Address)
                Log.e("loc",""+Address)
                editor.commit()
            }
        } catch (e: IOException) {
            e.message?.let { Log.e("tag", it) }
        }
        return result.toString()
    }


}