package com.supernal.oy.user.ui

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.adapters.bids_adapter
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentUserNotificationsBinding
import com.supernal.oy.models.userbiddetailDefaultResponse
import com.supernal.oy.models.userbiddetailListResponse
import com.supernal.oy.models.userbiddetailPopupResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserNotificationFragment : Fragment() {
    private var _binding: FragmentUserNotificationsBinding? = null
    lateinit var sharedPreferences: SharedPreferences
    lateinit var servicebidsAdapter: bids_adapter
    lateinit var token: String
    lateinit var bidsListResponse: List<userbiddetailListResponse>
    var bidid: Int = 0

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUserNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        sharedPreferences = requireContext().getSharedPreferences(
            "loginprefs", Context.MODE_PRIVATE
        )

        token = sharedPreferences.getString("token", "").toString()

        binding.bidsRecycleview.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayout.VERTICAL, false)

        getbidslist()
        return root
    }

    private fun getbidslist() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//


                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)


                val call =
                    apiServices.getuserbids(
                        "Token $token",
                    )

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<userbiddetailDefaultResponse> {
                    override fun onResponse(
                        call: Call<userbiddetailDefaultResponse>,
                        response: Response<userbiddetailDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())

                            bidsListResponse = response.body()?.data!!
                            if (bidsListResponse.isEmpty()) {

                                binding.bidsRecycleview.visibility = View.GONE
                                binding.nodata.visibility = View.VISIBLE
                            } else {

                                servicebidsAdapter = bids_adapter(
                                    requireActivity(),
                                    bidsListResponse
                                )
                                binding.bidsRecycleview.adapter =
                                    servicebidsAdapter
                                servicebidsAdapter.notifyDataSetChanged()

                                binding.bidsRecycleview.addOnItemClickListener(object :
                                    OnItemClickListener {
                                    override fun onItemClicked(position: Int, view: View) {
                                        // Your logic

                                        bidid = servicebidsAdapter.list.get(position).id

                                        Log.e("id", "" + bidid)

                                        val dialog = Dialog(context!!)
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                                        dialog.setCancelable(true)
                                        dialog.setContentView(R.layout.confirmbid_adapter)
                                        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                                        val cancel =
                                            dialog.findViewById(R.id.cancelbutton) as ImageView
                                        val confirm =
                                            dialog.findViewById(R.id.confirmorder) as Button

                                        val profilepicture =
                                            dialog.findViewById(R.id.profilepicture) as ImageView


                                        val jobtittle =
                                            dialog.findViewById(R.id.category) as TextView

                                        val rating =
                                            dialog.findViewById(R.id.rating) as TextView

                                        val servicepersonname =
                                            dialog.findViewById(R.id.pname) as TextView

                                        val amount =
                                            dialog.findViewById(R.id.bidamount) as TextView


                                        try {

                                            if (activity?.let {
                                                    NetWorkConection.isNEtworkConnected(
                                                        requireActivity()
                                                    )
                                                }!!) {


                                                //Set the Adapter to the RecyclerView//


                                                var apiServices =
                                                    ApiClient.client.create(Api::class.java)

                                                Log.e("API : ", "" + apiServices)


                                                val call =
                                                    apiServices.getservicepopup(
                                                        "Token $token", bidid.toString()
                                                    )

                                                Log.e("API Categories : ", "" + call.toString())
                                                Log.e("API token : ", "" + token)
                                                Log.e("API bidid : ", "" + bidid)


                                                call.enqueue(object :
                                                    Callback<userbiddetailPopupResponse> {
                                                    override fun onResponse(
                                                        call: Call<userbiddetailPopupResponse>,
                                                        response: Response<userbiddetailPopupResponse>
                                                    ) {

                                                        Log.e(
                                                            "API res : ",
                                                            "" + response.body().toString()
                                                        )

                                                        if (response.isSuccessful) {
                                                            Log.e(
                                                                "user response",
                                                                "" + response.body()?.toString()
                                                            )


                                                            var profileimage =
                                                                response.body()?.data!!.services.profile_pic

                                                            Glide.with(context!!).load(profileimage)
                                                                .apply(RequestOptions().fitCenter())
                                                                .error(R.drawable.electrician)
                                                                .into(profilepicture)
                                                            val personname =
                                                                response.body()?.data!!.services.fullname

                                                            servicepersonname.text = personname

                                                            val tittle =
                                                                response.body()?.data!!.job_id.job_title
                                                            jobtittle.text = tittle

                                                            val star =
                                                                response.body()?.data!!.job_id.job_rating

                                                            rating.text = star
                                                            val bidamount =
                                                                response.body()?.data!!.bid_amount
                                                            Log.e(
                                                                "amm",
                                                                "" + bidamount
                                                            )

                                                            amount.text = "" + bidamount


                                                        } else {
                                                            Toast.makeText(
                                                                activity,
                                                                response.message(),
                                                                Toast.LENGTH_LONG
                                                            ).show()

                                                            Log.e(
                                                                "response msg",
                                                                "" + response.message()
                                                            )
                                                            Log.e(
                                                                "respons body",
                                                                "" + response.body()
                                                            )
                                                            Log.e(
                                                                "respons code",
                                                                "" + response.code()
                                                            )
                                                        }
                                                    }

                                                    override fun onFailure(
                                                        call: Call<userbiddetailPopupResponse>,
                                                        t: Throwable?
                                                    ) {
                                                        Log.e("response", t.toString())
                                                    }
                                                })


                                            } else {


                                                Toast.makeText(
                                                    activity,
                                                    "Please Check your internet",
                                                    Toast.LENGTH_LONG
                                                ).show()
                                            }
                                        } catch (e: Exception) {
                                            e.printStackTrace()
                                            Toast.makeText(
                                                activity,
                                                "" + e.message,
                                                Toast.LENGTH_LONG
                                            ).show()

                                        } catch (e1: JSONException) {
                                            e1.printStackTrace()
                                        } catch (e2: NumberFormatException) {
                                            e2.printStackTrace()
                                        } catch (e3: NullPointerException) {
                                            e3.printStackTrace()
                                        }


                                        confirm.setOnClickListener {

                                            confirmservice()
                                            getbidslist()
                                            dialog.dismiss()
                                        }
                                        cancel.setOnClickListener { dialog.dismiss() }
                                        dialog.show()


                                    }
                                })


                            }

                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(
                        call: Call<userbiddetailDefaultResponse>,
                        t: Throwable?
                    ) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        } catch (e3: NullPointerException) {
            e3.printStackTrace()
        }


    }


    private fun confirmservice() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)


//                val call = apiServices.leadslist("Token $token")
                val call =
                    apiServices.confirmservice(
                        "Token $token",
                        bidid.toString()
                    )

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<userbiddetailPopupResponse> {
                    override fun onResponse(
                        call: Call<userbiddetailPopupResponse>,
                        response: Response<userbiddetailPopupResponse>
                    ) {


                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


//
                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<userbiddetailPopupResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }


    //on item click interface
    interface OnItemClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
        this.addOnChildAttachStateChangeListener(object :
            RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View) {
            }


            override fun onChildViewAttachedToWindow(view: View) {
                view.setOnClickListener {
                    val holder = getChildViewHolder(view)
                    onClickListener.onItemClicked(holder.adapterPosition, view)
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}