package com.supernal.oy.user.ui.payments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.supernal.oy.databinding.FragmentPaymentsBinding

class PaymentFragment : Fragment() {

    private var _binding: FragmentPaymentsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPaymentsBinding.inflate(inflater, container, false)

        val root: View = binding.root


        return root

    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}