package com.supernal.oy.user.ui

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentUserProfileBinding
import com.supernal.oy.models.PatchProfileDefaultResponse
import com.supernal.oy.utils.ManagePermissions
import com.supernal.oy.utils.NetWorkConection
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger

class MyProfileEditFragment : Fragment() {
    private var _binding: FragmentUserProfileBinding? = null
    private val binding get() = _binding!!
    lateinit var sharedPreferences: SharedPreferences
    lateinit var uname: String
    lateinit var ugender: String
    lateinit var uemail: String
    lateinit var umobile: String
    lateinit var uaddress: String
    private val STORAGE_REQUEST_CODE = 101
    private val CAMERA_REQUEST_CODE = 123
    private val PermissionsRequestCode = 123
    private var camerapath: String? = null
    private var picturePath: String? = null
    private var fileUri: Uri? = null
    private var imageString: String? = null
    private lateinit var managePermissions: ManagePermissions
    private var mImageFileLocation = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        _binding = FragmentUserProfileBinding.inflate(inflater, container, false)


        val list = listOf<String>(
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CALENDAR
        )


        var token = sharedPreferences.getString("token", "")
        var name = sharedPreferences.getString("fname", "")
        var g = sharedPreferences.getString("gender", "")
        var e = sharedPreferences.getString("email", "")
        var m = sharedPreferences.getString("mnumber", "")
        var l = sharedPreferences.getString("loc", "")
        var p = sharedPreferences.getString("pimage", "")



        Log.e("a", "" + name)
        Log.e("b", "" + g)
        Log.e("c", "" + e)
        Log.e("d", "" + m)
        Log.e("e", "" + l)

        binding.fullname.setText(name)
        binding.Gender.setText(g)
        binding.email.setText(e)
        binding.mobilenumber.setText(m)
        binding.userAddress.setText(l)


        Glide.with(context as Activity).load(p)
            .apply(RequestOptions().centerCrop())
            .error(R.drawable.electrician)
            .into(binding.profilepicUpload)



        binding.saveBtn.setOnClickListener {

            uname = binding.fullname.text.toString().trim()

            Log.e("kkkkk",""+uname)
            ugender = binding.Gender.text.toString().trim()
            uemail = binding.email.text.toString().trim()
            umobile = binding.mobilenumber.text.toString().trim()
            uaddress = binding.userAddress.text.toString().trim()

            patchprofile()

        }
        managePermissions =
            ManagePermissions(requireContext() as Activity, list, PermissionsRequestCode)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            managePermissions.checkPermissions()

        binding.profilepicUpload.setOnClickListener {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                managePermissions.checkPermissions()
            selectImage()


        }
        val root: View = binding.root
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun selectImage() {
        try {

            val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Add Photo!")
            builder.setItems(options) { dialog, item ->
                if (options[item] == "Take Photo") {


                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(intent, CAMERA_REQUEST_CODE)


                } else if (options[item] == "Choose from Gallery") {
                    val intent =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    startActivityForResult(intent, STORAGE_REQUEST_CODE)
                } else if (options[item] == "Cancel") {
                    dialog.dismiss()
                }
            }
            builder.show()
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (data != null) {


                if (resultCode == Activity.RESULT_OK) {
                    if (requestCode == CAMERA_REQUEST_CODE) {
                        val extras = data.extras
                        val imageBitmap = extras?.get("data") as Bitmap
                        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmSS").format(Date())
                        val imageFileName = timeStamp
                        // Here we specify the environment location and the exact path where we want                       to save the so-created file
                        val storageDirectory =
                            (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS))
                        Logger.getAnonymousLogger().info("Storage directory set")


                        val image = File.createTempFile(imageFileName, ".jpg", storageDirectory)

                        // Here the location is saved into the string mImageFileLocation
                        Logger.getAnonymousLogger().info("File name and path set")

                        var out: FileOutputStream? = null
                        try {
                            out = FileOutputStream(image)
                            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
                            out.flush()
                            out.close()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        camerapath = image.absolutePath
                        fileUri = Uri.parse(mImageFileLocation)
                        Log.e("camerauri", "" + fileUri)

                        binding.profilepicUpload.setImageBitmap(imageBitmap)


                        imageString = Base64.encodeToString(
                            getBytesFromBitmap(imageBitmap),
                            Base64.NO_WRAP
                        )


                        Log.e("b64", "" + imageString)

                        Log.e("imagename", "" + imageString)

//
                        // userImageUpdate(imageString)
                    } else if (requestCode == STORAGE_REQUEST_CODE) {
                        if (data != null) {
                            // Get the Image from data
                            val selectedImage = data.data
                            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                            val cursor = requireContext().contentResolver.query(
                                selectedImage!!,
                                filePathColumn,
                                null,
                                null,
                                null
                            )
                            assert(cursor != null)
                            cursor!!.moveToFirst()

                            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                            picturePath = cursor.getString(columnIndex)
                            // Set the Image in ImageView for Previewing the Media
                            var imageBitmap = BitmapFactory.decodeFile(picturePath)
                            binding.profilepicUpload.setImageBitmap(imageBitmap)
                            cursor.close()

                            Log.e("galleryPath", "" + picturePath)


                            imageString = Base64.encodeToString(
                                getBytesFromBitmap(imageBitmap),
                                Base64.NO_WRAP
                            )
                            Log.e("b64", "" + imageString)

                        }

                    }


                }


            } else {
                Toast.makeText(activity as Activity, "Select Image...", Toast.LENGTH_LONG)
                    .show()

            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    fun getBytesFromBitmap(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream)
        return stream.toByteArray()
    }

    private fun patchprofile() {

        if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


//            progressBarlogin.visibility = View.VISIBLE
            var token = sharedPreferences.getString("token", "")


            var apiServices = ApiClient.client.create(Api::class.java)

//
            val call =
                apiServices.patchprofile(
                    "Token $token",uname,umobile,uemail,"data:image/png;base64," +imageString!!,ugender,uaddress
                    )
            Log.e("t", "" + token)


            call.enqueue(object : Callback<PatchProfileDefaultResponse> {
                override fun onResponse(
                    call: Call<PatchProfileDefaultResponse>,
                    response: Response<PatchProfileDefaultResponse>

                ) {

//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, response.toString())

                    val status = response.body()?.status
                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        try {

                            val navController = Navigation.findNavController(
                                requireActivity(),
                                R.id.nav_host_fragment_content_oy_user
                            )
                            navController.navigate(R.id.nav_user_profile)


                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }

                    } else {

                        Toast.makeText(activity, "Invalid call", Toast.LENGTH_LONG).show()


                    }

                }


                override fun onFailure(call: Call<PatchProfileDefaultResponse>, t: Throwable?) {
                    Log.e("response", t.toString())
                }
            })


        } else {
            Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }

}