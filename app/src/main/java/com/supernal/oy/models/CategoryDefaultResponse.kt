package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class CategoryDefaultResponse(

    @SerializedName("message") val message : String,
    @SerializedName("status") val status : Boolean,
    @SerializedName("data") val data : List<CategoryListResponse>
)