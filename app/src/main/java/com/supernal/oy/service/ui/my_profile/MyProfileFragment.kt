package com.supernal.oy.service.ui.my_profile

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.databinding.FragmentProfileBinding
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.models.service_profile_defaultResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyProfileFragment : Fragment() {
    private lateinit var myProfileViewModel: MyProfileViewModel
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    lateinit var review_text:TextView
    lateinit var servicesharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myProfileViewModel =
        ViewModelProvider(this).get(MyProfileViewModel::class.java)
        _binding = FragmentProfileBinding.inflate(inflater, container, false)

        servicesharedPreferences =
            requireActivity().getSharedPreferences(
                "serviceloginprefs",
                Context.MODE_PRIVATE
            )
        val root: View = binding.root

        getusersProfile()

        return root
    }

    private fun getusersProfile() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)

                var token = servicesharedPreferences.getString("servicetoken", "")
                Log.e("ptoken", "" + token)

                val call = apiServices.getservicerprofile("Token $token")

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<service_profile_defaultResponse> {
                    override fun onResponse(
                        call: Call<service_profile_defaultResponse>,
                        response: Response<service_profile_defaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


                            val pfname: String? =
                                response.body()!!.data.fullname
                            val pemail: String? = response.body()!!.data.email
                            val pmobile: String? = response.body()!!.data.mobile
                            val paddress: String? = response.body()!!.data.address
                            val pimage: String? = response.body()!!.data.profile_pic
                            val pjobs: String? = response.body()!!.data.total_jobs.toString()
                            val pexp: String? = response.body()!!.data.experience.toString()



                            val cat=response.body()!!.data.category.map {

                                val catgorynames=it.name
                                binding.category.text=catgorynames

                            }

                            binding.providerName.text=pfname
                            binding.email.text=pemail
                            binding.mobile.text=pmobile
                            binding.address.text=paddress
                            binding.totaljobs.text=pjobs
                            binding.serviceexpe.text=pexp


                            Glide.with(context!!).load(pimage)
                                .apply(RequestOptions().centerCrop())
                                .error(R.drawable.electrician)
                                .into(binding.profile)

//                            val editor = sharedPreferences.edit()
//                            editor.putString(
//                                "fname",
//                                response.body()!!.data.first_name + response.body()!!.data.last_name
//                            )
//                            editor.putString("gender", response.body()!!.data.gender)
//                            editor.putString("email", response.body()!!.data.email)
//                            editor.putString("mnumber", response.body()!!.data.mobile)
//                            editor.putString("loc", response.body()!!.data.address)
//                            editor.putString("pimage",response.body()!!.data.profile_pic)
//                            editor.commit()



                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<service_profile_defaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}