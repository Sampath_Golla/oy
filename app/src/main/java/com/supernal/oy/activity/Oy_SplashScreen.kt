package com.supernal.oy.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.supernal.oy.R
import com.supernal.oy.service.activity.Oy_Provider
import com.supernal.oy.user.activity.Oy_User

class Oy_SplashScreen : Activity() {
    lateinit var sharedPreferences: SharedPreferences
    lateinit var servicesharedPreferences: SharedPreferences

    var isLogined: Boolean = false
    var serviceisLogined: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_splash)



        sharedPreferences = getSharedPreferences("loginprefs", Context.MODE_PRIVATE)
        servicesharedPreferences = getSharedPreferences("serviceloginprefs", Context.MODE_PRIVATE)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            );
        }
        Handler().postDelayed({


            isLogined = sharedPreferences.getBoolean("islogined", false)
            serviceisLogined = servicesharedPreferences.getBoolean("islogin", false)

            if (isLogined) {
                val intent = Intent(this, Oy_User::class.java)
                startActivity(intent)
                finish()

            } else if (serviceisLogined) {

                val intent = Intent(this, Oy_Provider::class.java)
                startActivity(intent)
                finish()

            } else {
                intent = Intent(this, Oy_Introduction::class.java)
                startActivity(intent)
                finish()
            }
        }, 2000)
    }
}