package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class Cancel_ReasonResponse(

    @SerializedName("id") val id : Int,
    @SerializedName("text") val text : String
)