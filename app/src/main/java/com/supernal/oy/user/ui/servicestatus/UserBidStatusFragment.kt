package com.supernal.oy.user.ui.servicestatus

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.supernal.oy.adapters.userservicestatus_adapter
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentUserBidstatusBinding
import com.supernal.oy.models.userbiddetailDefaultResponse
import com.supernal.oy.models.userbiddetailListResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserBidStatusFragment : Fragment() {
    private var _binding: FragmentUserBidstatusBinding? = null
    lateinit var sharedPreferences: SharedPreferences
    lateinit var servicestatusAdapter: userservicestatus_adapter
    lateinit var token: String
    lateinit var servstatusListResponse: List<userbiddetailListResponse>

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUserBidstatusBinding.inflate(inflater, container, false)
        val root: View = binding.root

        sharedPreferences = requireContext().getSharedPreferences(
            "loginprefs", Context.MODE_PRIVATE
        )

        token = sharedPreferences.getString("token", "").toString()

        binding.userserviceStatusRecycleview.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayout.VERTICAL, false)
        userservicestatus()
        return root
    }


    private fun userservicestatus() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)


//                val call = apiServices.leadslist("Token $token")
                val call =
                    apiServices.getuserservicestatus(
                        "Token $token",
                    )

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<userbiddetailDefaultResponse> {
                    override fun onResponse(
                        call: Call<userbiddetailDefaultResponse>,
                        response: Response<userbiddetailDefaultResponse>
                    ) {


                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())

                            servstatusListResponse = response.body()?.data!!



                            if (servstatusListResponse.isEmpty()) {

                                binding.userserviceStatusRecycleview.visibility = View.GONE
                                binding.nodata.visibility = View.VISIBLE
                            } else {

                                servicestatusAdapter = userservicestatus_adapter(
                                    requireActivity(),
                                    servstatusListResponse
                                )
                                binding.userserviceStatusRecycleview.adapter =
                                    servicestatusAdapter
                                servicestatusAdapter.notifyDataSetChanged()
                            }

                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(
                        call: Call<userbiddetailDefaultResponse>,
                        t: Throwable?
                    ) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}