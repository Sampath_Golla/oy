package com.supernal.oy.user.ui.profile

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.ProfileBinding
import com.supernal.oy.models.ProfileDefaultResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class UserProfileFragment : Fragment() {
    private var _binding: ProfileBinding? = null
    lateinit var sharedPreferences: SharedPreferences


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        sharedPreferences =
            requireActivity().getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        getusersProfile()
        // Inflate the layout for this fragment
        _binding = ProfileBinding.inflate(inflater, container, false)
        val root: View = binding.root


        binding.editprofile.setOnClickListener {

            val navController = Navigation.findNavController(
                requireActivity(),
                R.id.nav_host_fragment_content_oy_user
            )
            navController.navigate(R.id.nav_user_edit_profile)
        }
        return root


    }

    private fun getusersProfile() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)

                var token = sharedPreferences.getString("token", "")
                Log.e("ptoken", "" + token)

                val call = apiServices.getuserprofile("Token $token")

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<ProfileDefaultResponse> {
                    override fun onResponse(
                        call: Call<ProfileDefaultResponse>,
                        response: Response<ProfileDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


                            val pfname: String? =
                                response.body()!!.data.first_name + response.body()!!.data.last_name
                            val pflast: String? = response.body()!!.data.last_name
                            val pgender: String? = response.body()!!.data.gender
                            val pemail: String? = response.body()!!.data.email
                            val pmobile: String? = response.body()!!.data.mobile
                            val paddress: String? = response.body()!!.data.address
                            val pimage: String? = response.body()!!.data.profile_pic

                            binding.fullname.text = pfname + " " + pflast
                            binding.gender.text = pgender
                            binding.emailid.text = pemail
                            binding.mobilenumber.text = pmobile
                            binding.address.text = paddress


                            Glide.with(context!!).load(pimage)
                                .apply(RequestOptions().centerCrop())
                                .error(R.drawable.electrician)
                                .into(binding.profileimage)

                            val editor = sharedPreferences.edit()
                            editor.putString(
                                "fname",
                                response.body()!!.data.first_name + response.body()!!.data.last_name
                            )
                            editor.putString("gender", response.body()!!.data.gender)
                            editor.putString("email", response.body()!!.data.email)
                            editor.putString("mnumber", response.body()!!.data.mobile)
                            editor.putString("loc", response.body()!!.data.address)
                            editor.putString("pimage",response.body()!!.data.profile_pic)
                            editor.commit()



                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<ProfileDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}