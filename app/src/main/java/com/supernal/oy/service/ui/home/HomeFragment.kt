package com.supernal.oy.service.ui.home

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.supernal.oy.adapters.services_adapter
import android.content.Context
import android.widget.AdapterView
import android.widget.Toast
import androidx.navigation.Navigation
import com.supernal.oy.R
import com.supernal.oy.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    lateinit var gridView: GridView
    private var servicesNames = arrayOf("Leads", "My Jobs", "Payment", "History")
    private var serviceImages = intArrayOf(
        R.drawable.leads, R.drawable.my_jobs, R.drawable.payments,
        R.drawable.history,
    )
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @SuppressLint("ServiceCast")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        gridView=binding.gridView
        if (layoutInflater == null) {
            val layoutInflater =
                context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        val mainAdapter= services_adapter(this@HomeFragment ,servicesNames,serviceImages)
        gridView.adapter=mainAdapter
        gridView.onItemClickListener = AdapterView.OnItemClickListener { parent, v, position, id ->

            when(position){
                0-> {
                    val navController =
                        Navigation.findNavController(context as Activity, R.id.nav_host_fragment_content_oy_provider)
                    navController.navigate(R.id.nav_leads)
                }
                1-> {
                    val navController =
                        Navigation.findNavController(context as Activity, R.id.nav_host_fragment_content_oy_provider)
                    navController.navigate(R.id.nav_myjobs)
                }
                2-> {
                    val navController =
                        Navigation.findNavController(context as Activity, R.id.nav_host_fragment_content_oy_provider)
                    navController.navigate(R.id.nav_payments)
                }
                3-> {
                    val navController =
                        Navigation.findNavController(context as Activity, R.id.nav_host_fragment_content_oy_provider)
                    navController.navigate(R.id.nav_job_history)
                }
                else->{
                    Toast.makeText(context, "Currently this service is unavailable", Toast.LENGTH_SHORT).show()
                }
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}