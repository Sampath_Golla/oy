package com.supernal.oy.adapters

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.models.CategoryListResponse


class Category_adapter(var context: Context, var list: List<CategoryListResponse>) :
    RecyclerView.Adapter<Category_adapter.MyViewHolder>() {

    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var category_ist: List<CategoryListResponse>

    //inflating and returning our view holder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        servicesharedPreferences =context.getSharedPreferences("serviceloginprefs",Context.MODE_PRIVATE)

        val view = LayoutInflater.from(context)
            .inflate(R.layout.category_adapter, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {


        return list.size


    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


        holder.categoryname.text =  list.get(position).name
        var categoryimage = list.get(position).logo

        Glide.with(context).load(categoryimage)
            .apply(RequestOptions().fitCenter())
            .error(R.drawable.electrician)
            .into(holder.categoryimage)
    }


    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {


        val categoryimage = itemView!!.findViewById(R.id.categoryimage) as ImageView
        val categoryname = itemView!!.findViewById(R.id.categoryname) as TextView


    }
}