package com.supernal.oy.service.ui.bid

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentBidBinding
import com.supernal.oy.models.BidDefaultResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BidFragment : Fragment() {

    private lateinit var bidviewModel: BidViewModel
    private var _binding: FragmentBidBinding? = null
    lateinit var bid_button: Button
    lateinit var servicesharedPreferences: SharedPreferences
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bidviewModel =
            ViewModelProvider(this).get(BidViewModel::class.java)
        _binding = FragmentBidBinding.inflate(inflater, container, false)
        val root: View = binding.root
        bid_button = binding.bidButton
        servicesharedPreferences = requireContext().getSharedPreferences(
            "serviceloginprefs", Context.MODE_PRIVATE
        )

        bid_button.setOnClickListener {
            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_oy_provider
                )
            navController.navigate(R.id.nav_Bid_Request)
        }

        getleadinfo()

        return root
    }

    private fun getleadinfo() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = servicesharedPreferences.getString("servicetoken", "")
                var leadid = servicesharedPreferences.getInt("leadid", 0)

                Log.e("API : ", "" + apiServices)


//                val call = apiServices.leadslist("Token $token")
                val call =
                    apiServices.leadsdetailist(
                        "Token $token",
                        leadid
                    )

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<BidDefaultResponse> {
                    override fun onResponse(
                        call: Call<BidDefaultResponse>,
                        response: Response<BidDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


                            binding.providerName.text = response.body()!!.data.full_name
                            binding.posteddatetime.text =
                                " Posted on  " + response.body()!!.data.posted_on

                            binding.detailDesc.text = response.body()!!.data.job_description
                            binding.timings.text =
                                response.body()!!.data.date_scheduled + " , " + response.body()!!.data.time_scheduled
                            binding.locationDetails.text = response.body()!!.data.address

                            var serviceimage = response.body()!!.data.job_image
                            var profilepic = response.body()!!.data.profile_pic

                            context?.let {
                                Glide.with(it).load(profilepic)
                                    .apply(RequestOptions().fitCenter())
                                    .error(R.drawable.electrician)
                                    .into(binding.profileImg)
                            }

                            context?.let {
                                Glide.with(it).load(serviceimage)
                                    .apply(RequestOptions().fitCenter())
                                    .error(R.drawable.electrician)
                                    .into(binding.serviceimageview)
                            }

                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<BidDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}