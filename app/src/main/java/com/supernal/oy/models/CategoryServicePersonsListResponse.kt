package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class CategoryServicePersonsListResponse(

    @SerializedName("id") val id : Int,
    @SerializedName("profile_pic") val profile_pic : String,
    @SerializedName("fullname") val fullname : String,
    @SerializedName("experience") val experience : String,
    @SerializedName("mobile") val mobile : String,
    @SerializedName("email") val email : String,
    @SerializedName("category") val category : List<CategoryListResponse>,
    @SerializedName("language") val language : String,
    @SerializedName("latitude") val latitude : Double,
    @SerializedName("longitude") val longitude : Double,
    @SerializedName("address") val address : String,
    @SerializedName("avg_stars") val avg_stars : String,
    @SerializedName("avg_rating") val avg_rating : String,
    @SerializedName("total_reviews") val total_reviews : String,
    @SerializedName("total_jobs") val total_jobs : String

)