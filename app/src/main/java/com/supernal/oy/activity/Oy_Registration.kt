package com.supernal.oy.activity

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import com.supernal.oy.R
import com.supernal.oy.adapters.Reasons_adapter
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.models.*
import com.supernal.oy.utils.ManagePermissions
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger


class Oy_Registration : Activity() {

    lateinit var token: String
    lateinit var fullname: EditText
    lateinit var mobilenumber: EditText
    lateinit var email: EditText
    lateinit var addhaarnumber: EditText
    lateinit var experience: EditText
    lateinit var navigation_btn: ImageView
    lateinit var filepath_addhar: TextView
    lateinit var filepath_strng: String
    lateinit var filepath_addha2: TextView
    lateinit var name: String
    lateinit var mobile: String
    lateinit var emailid: String
    lateinit var addhaarno: String
    lateinit var exp: String
    lateinit var proof1: Button
    lateinit var proof2: Button
    lateinit var category_spinner: Button
    private val STORAGE_REQUEST_CODE = 101
    private val CAMERA_REQUEST_CODE = 123
    private val PermissionsRequestCode = 123
    private var camerapath: String? = null
    private var picturePath: String? = null
    private var fileUri: Uri? = null
    private var imageString: String? = null
    private lateinit var managePermissions: ManagePermissions
    private var mImageFileLocation = ""
    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var categoryList: List<CategoryListResponse>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_registration)

        servicesharedPreferences = getSharedPreferences("serviceloginprefs", Context.MODE_PRIVATE)
        token = servicesharedPreferences.getString("servicetoken", "").toString()

        fullname = findViewById(R.id.fullname) as EditText
        mobilenumber = findViewById(R.id.mobilenumber) as EditText
        email = findViewById(R.id.email) as EditText
        addhaarnumber = findViewById(R.id.addhaarnumber) as EditText
        experience = findViewById(R.id.experience) as EditText
        navigation_btn = findViewById(R.id.navigation_btn) as ImageView
        proof1 = findViewById(R.id.proof1) as Button
        proof2 = findViewById(R.id.proof2) as Button
        filepath_addhar = findViewById(R.id.filepath_addhar) as TextView
        filepath_addha2 = findViewById(R.id.filepath_addhar2) as TextView

        val list = listOf<String>(
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CALENDAR
        )


        managePermissions = ManagePermissions(this, list, PermissionsRequestCode)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            managePermissions.checkPermissions()
        navigation_btn.setOnClickListener {


            name = fullname.text.toString().trim()
            mobile = mobilenumber.text.toString().trim()
            emailid = email.text.toString().trim()
            addhaarno = addhaarnumber.text.toString().trim()
            exp = experience.text.toString().trim()
            filepath_strng = filepath_addhar.text.toString().trim()

            if (name.isEmpty()) {
                fullname.error = "Full name"
                fullname.requestFocus()
            } else if (mobile.isEmpty()) {
                mobilenumber.error = "mobile number"
                mobilenumber.requestFocus()
            } else if (emailid.isEmpty()) {
                email.error = "email"
                email.requestFocus()
            } else if (addhaarno.isEmpty() || (addhaarno.length > 12)) {
                addhaarnumber.error = "Addhaar number"
                addhaarnumber.requestFocus()
            } else if (exp.isEmpty()) {
                experience.error = "Experience"
                experience.requestFocus()
            } else if (filepath_strng.isEmpty()) {
                filepath_addhar.error = "Add Image"
                filepath_addhar.requestFocus()
            } else
                patchservicepersondata()
        }


        proof1.setOnClickListener {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                managePermissions.checkPermissions()
            selectImage()

        }
    }

    private fun selectImage() {
        try {

            val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Add Photo!")
            builder.setItems(options) { dialog, item ->
                if (options[item] == "Take Photo") {


                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(intent, CAMERA_REQUEST_CODE)


                } else if (options[item] == "Choose from Gallery") {
                    val intent =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    startActivityForResult(intent, STORAGE_REQUEST_CODE)
                } else if (options[item] == "Cancel") {
                    dialog.dismiss()
                }
            }
            builder.show()
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (data != null) {


                if (resultCode == Activity.RESULT_OK) {
                    if (requestCode == CAMERA_REQUEST_CODE) {
                        val extras = data.extras
                        val imageBitmap = extras?.get("data") as Bitmap
                        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmSS").format(Date())
                        val imageFileName = timeStamp
                        // Here we specify the environment location and the exact path where we want                       to save the so-created file
                        val storageDirectory =
                            (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS))
                        Logger.getAnonymousLogger().info("Storage directory set")


                        val image = File.createTempFile(imageFileName, ".jpg", storageDirectory)

                        // Here the location is saved into the string mImageFileLocation
                        Logger.getAnonymousLogger().info("File name and path set")

                        var out: FileOutputStream? = null
                        try {
                            out = FileOutputStream(image)
                            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
                            out.flush()
                            out.close()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        camerapath = image.absolutePath
                        fileUri = Uri.parse(mImageFileLocation)
                        Log.e("camerauri", "" + image.absoluteFile)
                        Log.e("cam", "" + image.name)

                        filepath_addhar.text = "" + image.name
//                        binding.uploadedImage.setImageBitmap(imageBitmap)


                        imageString = Base64.encodeToString(
                            getBytesFromBitmap(imageBitmap),
                            Base64.NO_WRAP
                        )


                        Log.e("b64", "" + imageString)

                        Log.e("imagename", "" + imageString)

//
                        // userImageUpdate(imageString)
                    } else if (requestCode == STORAGE_REQUEST_CODE) {
                        if (data != null) {
                            // Get the Image from data
                            val selectedImage = data.data
                            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                            val cursor = this.contentResolver.query(
                                selectedImage!!,
                                filePathColumn,
                                null,
                                null,
                                null
                            )
                            assert(cursor != null)
                            cursor!!.moveToFirst()

                            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                            picturePath = cursor.getString(columnIndex)
                            // Set the Image in ImageView for Previewing the Media
                            var imageBitmap = BitmapFactory.decodeFile(picturePath)
//                            binding.uploadedImage.setImageBitmap(imageBitmap)
                            cursor.close()

                            Log.e("galleryPath", "" + picturePath)

                            val filename: String =
                                picturePath!!.substring(picturePath!!.lastIndexOf("/") + 1)


                            filepath_addhar.text = filename

                            Log.e("filename", "" + filename)


                            imageString = Base64.encodeToString(
                                getBytesFromBitmap(imageBitmap),
                                Base64.NO_WRAP
                            )
                            Log.e("b64", "" + imageString)

                        }

                    }


                }


            } else {
                Toast.makeText(this, "Select Shop Image...", Toast.LENGTH_LONG)
                    .show()

            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    fun getBytesFromBitmap(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream)
        return stream.toByteArray()
    }

    private fun patchservicepersondata() {

        if (NetWorkConection.isNEtworkConnected(this)) {


//            progressBarlogin.visibility = View.VISIBLE


            var apiServices = ApiClient.client.create(Api::class.java)

//            val call =
//                apiServices.rejisterserviceperson(
//                    "Token $token",name,mobile,emailid,addhaarno,"","","1",exp
//                )
            val call =
                apiServices.rejisterserviceperson(
                    "Token $token",
                    name,
                    mobile,
                    emailid,
                    "data:image/png;base64," + imageString, addhaarno, "", "", "2",exp)
            Log.e("t", "" + token)


            call.enqueue(object : Callback<RejistrationDefaultResponse> {
                override fun onResponse(
                    call: Call<RejistrationDefaultResponse>,
                    response: Response<RejistrationDefaultResponse>

                ) {

//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, response.toString())

                    val status = response.body()?.status
                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        try {

                            val intent =
                                Intent(
                                    this@Oy_Registration,
                                    Oy_Verification::class.java
                                )
                            startActivity(intent)

//                            val editor = sharedPreferences.edit()
//                            editor.putString("token", response.body()?.data!!.token.toString())
//                            Log.e("mtoken",""+response.body()?.data!!.token.toString())
//                            editor.commit()

                            //postOtp()
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }

                    } else {
                        Toast.makeText(
                            this@Oy_Registration,
                            "Invalid token ",
                            Toast.LENGTH_LONG
                        ).show()

                    }

                }


                override fun onFailure(call: Call<RejistrationDefaultResponse>, t: Throwable) {
//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, t.toString())
                    Toast.makeText(
                        this@Oy_Registration,
                        "Invalid ",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })


        } else {
            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }

//    private fun getreasonlist() {
//
//
//        try {
//
//            if (this.let { NetWorkConection.isNEtworkConnected(it) }!!) {
//
//
//                //Set the Adapter to the RecyclerView//
//
//                var apiServices = ApiClient.client.create(Api::class.java)
//
//
//                Log.e("API : ", "" + apiServices)
//
//
//                val call = apiServices.getreasonslist()
//
//                Log.e("API Categories : ", "" + call)
//
//                call.enqueue(object : Callback<Cancel_Reason_DefaultResponse> {
//                    override fun onResponse(
//                        call: Call<Cancel_Reason_DefaultResponse>,
//                        response: Response<Cancel_Reason_DefaultResponse>
//                    ) {
//
//                        if (response.isSuccessful) {
//                            Log.e("user response", "" + response.body()?.toString())
//
//                          val  categoryList = response.body()!!.data
//
//                            reasonadapter = Reasons_adapter(activi(), categoryList)
//
//                            binding.reasonSpinner.adapter=reasonadapter
//                            reasonadapter.notifyDataSetChanged()
//
//
//                        } else {
//                            Toast.makeText(this@Oy_Registration, "No Data", Toast.LENGTH_LONG).show()
//
//
//                        }
//                    }
//
//                    override fun onFailure(
//                        call: Call<Cancel_Reason_DefaultResponse>,
//                        t: Throwable?
//                    ) {
//                        Log.e("response", t.toString())
//                    }
//                })
//
//
//            } else {
//
//
//                Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//            Toast.makeText(this, "" + e.message, Toast.LENGTH_LONG).show()
//
//        } catch (e1: JSONException) {
//            e1.printStackTrace()
//        } catch (e2: NumberFormatException) {
//            e2.printStackTrace()
//        }
//
//
//    }



}





