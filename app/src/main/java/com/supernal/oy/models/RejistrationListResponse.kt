package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class RejistrationListResponse(

    @SerializedName("mobile") val mobile : String,
    @SerializedName("fullname") val fullname : String,
    @SerializedName("email") val email : String,
    @SerializedName("profile_pic") val profile_pic : String,
    @SerializedName("addhaar_number") val addhaar_number : String,
    @SerializedName("addhaar_front_image") val addhaar_front_image : String,
    @SerializedName("addhaar_back_image") val addhaar_back_image : String,
    @SerializedName("category") val category : List<Int>,
    @SerializedName("experience") val experience : String
)