package com.supernal.oy.service.ui.bid_request

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BidRequestViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Bid Request Fragment"
    }
    val text: LiveData<String> = _text
}