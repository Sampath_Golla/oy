package com.supernal.oy.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.models.VerificationDefaultResponse
import com.supernal.oy.service.activity.Oy_ProviderLocation
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Oy_Verification : Activity() {
    lateinit var navigation_home: ImageView
    lateinit var sharedPreferences: SharedPreferences
    lateinit var token: String
    lateinit var view1: View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_verification_screen)

        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        token = sharedPreferences.getString("token", "").toString()
        view1 = findViewById(R.id.view1)
        navigation_home = findViewById(R.id.navigate_home)
        navigation_home.setOnClickListener {
            startActivity(Intent(this@Oy_Verification, Oy_ProviderLocation::class.java))
        }
        getuserverificationstatus()
    }


    private fun getuserverificationstatus() {


        try {

            if (NetWorkConection.isNEtworkConnected(this)) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)

                Log.e("ptoken", "" + token)

//                val call = apiServices.getuserverificationstatus("Token $token")
                val call =
                    apiServices.getuserverificationstatus("Token $token")

                Log.e("API verified : ", "" + call)

                call.enqueue(object : Callback<VerificationDefaultResponse> {
                    override fun onResponse(
                        call: Call<VerificationDefaultResponse>,
                        response: Response<VerificationDefaultResponse>
                    ) {
                        Log.e("code", "" + response.body()?.code)
                        if (response.body()!!.code.equals("0") || response.body()!!.code == 0) {



                            view1.visibility = View.GONE
                            navigation_home.visibility = View.GONE

                            Log.e("user response", "" + response.body()?.toString())


                        } else {


                            view1.visibility = View.VISIBLE
                            navigation_home.visibility = View.VISIBLE

                            Toast.makeText(this@Oy_Verification, "No Data", Toast.LENGTH_LONG)
                                .show()


                        }
                    }

                    override fun onFailure(call: Call<VerificationDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }catch (e1: NullPointerException) {
            e1.printStackTrace()
        }


    }

}