package com.supernal.oy.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.supernal.oy.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}