package com.supernal.oy.service.ui.bid_request

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentBidRequestBinding
import com.supernal.oy.models.BidamountDefaultResponse
import com.supernal.oy.models.BidamountListResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BidRequestFragment : Fragment() {

    private lateinit var bidRequestViewModel: BidRequestViewModel
    private var _binding: FragmentBidRequestBinding? = null
    lateinit var sent_btn: Button
    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var bidamount: String
    lateinit var amountlist: List<BidamountListResponse>


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bidRequestViewModel =
            ViewModelProvider(this).get(BidRequestViewModel::class.java)
        _binding = FragmentBidRequestBinding.inflate(inflater, container, false)
        servicesharedPreferences = requireContext().getSharedPreferences(
            "serviceloginprefs", Context.MODE_PRIVATE
        )
        val root: View = binding.root
        sent_btn = binding.sentBtn


        sent_btn.setOnClickListener {
            bidamount = binding.bidEditTxt.text.toString().trim()

            if (bidamount.isEmpty()) {
                binding.bidEditTxt.error = "Bid Amount"
                binding.bidEditTxt.requestFocus()
            } else {
                postbidamount()

            }

        }

        return root
    }


    private fun postbidamount() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = servicesharedPreferences.getString("servicetoken", "")
                var leadid = servicesharedPreferences.getInt("leadid", 0)

                Log.e("API : ", "" + apiServices)


//                val call = apiServices.leadslist("Token $token")
                val call =
                    apiServices.postbidamount(
                        "Token $token",
                        leadid, bidamount
                    )

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<BidamountDefaultResponse> {
                    override fun onResponse(
                        call: Call<BidamountDefaultResponse>,
                        response: Response<BidamountDefaultResponse>
                    ) {

                        if (response.body()!!.code==1) {
                            Log.e("user response", "" + response.body()?.toString())

                            Toast.makeText(activity, "Bid Successful", Toast.LENGTH_LONG).show()

                            val navController =
                                Navigation.findNavController(
                                    context as Activity,
                                    R.id.nav_host_fragment_content_oy_provider
                                )
                            navController.navigate(R.id.nav_home)

                        } else {
                            Toast.makeText(activity, ""+response.body()?.message, Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<BidamountDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}