package com.supernal.oy.service.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.supernal.oy.R
import com.supernal.oy.adapters.Leads_adapter
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentLeadsBinding
import com.supernal.oy.models.LeadsDefaultListResponse
import com.supernal.oy.models.LeadsDefaultResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class LeadsFragment : Fragment() {
    private var _binding: FragmentLeadsBinding? = null
    lateinit var leadsAdapter: Leads_adapter
    lateinit var token: String
    lateinit var leadsListResponse: List<LeadsDefaultListResponse>
    lateinit var servicesharedPreferences: SharedPreferences
    var datepicked: String? = ""

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLeadsBinding.inflate(inflater, container, false)

        servicesharedPreferences = requireContext().getSharedPreferences(
            "serviceloginprefs", Context.MODE_PRIVATE
        )

        token = servicesharedPreferences.getString("servicetoken", "").toString()

        binding.leadsRecycleview.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayout.VERTICAL, false)
        val root: View = binding.root

        binding.datepicker.text = SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis())

        var cal = Calendar.getInstance()

        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                val myFormat = "yyyy-MM-dd" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                binding.datepicker.text = sdf.format(cal.time)
                datepicked = sdf.format(cal.time)
                getleadslist(datepicked!!)

            }
        binding.datepicker.setOnClickListener {
            val datepickerdialog = DatePickerDialog(
                context as Activity, dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )
            datepickerdialog.datePicker.setMinDate(System.currentTimeMillis() - 1000)
            datepickerdialog.show()

        }


        getleadslist(datepicked!!)
        return root
    }

    //on item click interface
    interface OnItemClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
        this.addOnChildAttachStateChangeListener(object :
            RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View) {
            }


            override fun onChildViewAttachedToWindow(view: View) {
                view.setOnClickListener {
                    val holder = getChildViewHolder(view)
                    onClickListener.onItemClicked(holder.adapterPosition, view)
                }
            }
        })
    }

    private fun getleadslist(datestring: String) {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)


//                val call = apiServices.leadslist("Token $token")
                val call =
                    apiServices.leadslist(
                        "Token $token",
                        datestring
                    )

                Log.e("API Categories : ", "" + call)
                Log.e("token : ", "" + token)


                call.enqueue(object : Callback<LeadsDefaultResponse> {
                    override fun onResponse(
                        call: Call<LeadsDefaultResponse>,
                        response: Response<LeadsDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())

                            leadsListResponse = response.body()?.data!!
                            if (leadsListResponse.isEmpty()) {

                                binding.leadsRecycleview.visibility = View.GONE
                                binding.nodata.visibility = View.VISIBLE
                            } else {

                                leadsAdapter = Leads_adapter(
                                    requireActivity(),
                                    leadsListResponse
                                )
                                binding.leadsRecycleview.adapter =
                                    leadsAdapter
                                leadsAdapter.notifyDataSetChanged()

                                binding.leadsRecycleview.addOnItemClickListener(object :
                                    OnItemClickListener {
                                    override fun onItemClicked(position: Int, view: View) {
                                        // Your logic

                                        val editor = servicesharedPreferences.edit()
//
                                        val leadid = leadsAdapter?.list?.get(position)!!.id

                                        editor.putInt("leadid", leadsAdapter?.list?.get(position)!!.id)
                                        editor.commit()

                                        Log.e("id",""+leadid)
                                        Log.e("leadid",""+leadid)

                                            val navController = Navigation.findNavController(
                                                activity!!,
                                                R.id.nav_host_fragment_content_oy_provider
                                            )
                                            navController.navigate(R.id.nav_bidjobs)



                                    }

                                })

                            }
//


                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<LeadsDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}