package com.supernal.oy.service.ui.my_jobs

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.supernal.oy.R
import com.supernal.oy.adapters.My_Jobs_List_adapter
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentMyJobsBinding
import com.supernal.oy.models.MyJobsDefaultResponse
import com.supernal.oy.models.MyJobsListResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyJobFragment : Fragment() {

    private lateinit var myjobviewModel: MyJobViewModel
    private var _binding: FragmentMyJobsBinding? = null
    lateinit var jobsAdapter: My_Jobs_List_adapter
    lateinit var token: String
    lateinit var jobsListResponse: List<MyJobsListResponse>
    lateinit var servicesharedPreferences: SharedPreferences

    private val binding get() = _binding!!
    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        myjobviewModel =
            ViewModelProvider(this).get(MyJobViewModel::class.java)
        _binding = FragmentMyJobsBinding.inflate(inflater, container, false)

        servicesharedPreferences = requireContext().getSharedPreferences(
            "serviceloginprefs", Context.MODE_PRIVATE
        )

        token = servicesharedPreferences.getString("servicetoken", "").toString()

        binding.myjobsRecycleview.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayout.VERTICAL, false)
        val root: View = binding.root

        getleadslist()
        return root
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    private fun getleadslist() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)


                val call =
                    apiServices.getservicepersonsjobslist(
                        "Token $token","")


                Log.e("API Categories : ", "" + call)
                Log.e("token : ", "" + token)


                call.enqueue(object : Callback<MyJobsDefaultResponse> {
                    override fun onResponse(
                        call: Call<MyJobsDefaultResponse>,
                        response: Response<MyJobsDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())

                            jobsListResponse = response.body()?.data!!
                            if (jobsListResponse.isEmpty()) {

                                binding.myjobsRecycleview.visibility = View.GONE
//                                binding.nodata.visibility = View.VISIBLE
                            } else {

                                jobsAdapter = My_Jobs_List_adapter(
                                    requireActivity(),
                                     jobsListResponse
                                )
                                binding.myjobsRecycleview.adapter =
                                    jobsAdapter
                                jobsAdapter.notifyDataSetChanged()

                                binding.myjobsRecycleview.addOnItemClickListener(object :
                                    MyJobFragment.OnItemClickListener {
                                    override fun onItemClicked(position: Int, view: View) {
                                        // Your logic

                                        val editor = servicesharedPreferences.edit()
//
                                        val jobid = jobsAdapter?.list?.get(position)!!.id

                                        editor.putInt("jobid", jobsAdapter?.list?.get(position)!!.id)
                                        editor.commit()

                                        Log.e("id",""+jobid)
                                        Log.e("leadid",""+jobid)

                                        val navController = Navigation.findNavController(
                                            activity!!,
                                            R.id.nav_host_fragment_content_oy_provider
                                        )
                                        navController.navigate(R.id.nav_myjobsindetail)



                                    }

                                })

                            }
//


                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<MyJobsDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }
    //on item click interface
    interface OnItemClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
        this.addOnChildAttachStateChangeListener(object :
            RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View) {
            }


            override fun onChildViewAttachedToWindow(view: View) {
                view.setOnClickListener {
                    val holder = getChildViewHolder(view)
                    onClickListener.onItemClicked(holder.adapterPosition, view)
                }
            }
        })
    }
}