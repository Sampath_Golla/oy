package com.supernal.oy.payment

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import com.razorpay.Checkout
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.models.JobratingDefaultResponse
import com.supernal.oy.models.createorder_DefaultResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Razorpay_Payment_activity : Activity(), PaymentResultWithDataListener {
    lateinit var sharedPreferences: SharedPreferences
    lateinit var username: TextView
    lateinit var price: TextView
    lateinit var ratingbar: RatingBar
    lateinit var token: String
    lateinit var rate: String
    lateinit var paynow: Button
    lateinit var razorpay_id: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_user_review_screen)
        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )
        val name = sharedPreferences.getString("name", "")
        val money = sharedPreferences.getString("amount", "")
        token = sharedPreferences.getString("token", "").toString()

        username = findViewById(R.id.provider_name_R)
        price = findViewById(R.id.price)
        paynow = findViewById(R.id.paynow)
        ratingbar = findViewById(R.id.ratingBar)
        username.text = name
        price.text = money
        paynow.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {

                rate = ratingbar.rating.toString()

                Log.e("ttt", "" + rate)
                Toast.makeText(
                    this@Razorpay_Payment_activity,
                    java.lang.String.valueOf(ratingbar.getRating()),
                    Toast.LENGTH_SHORT
                ).show()
                postrating()
                startPayment()
                createorder()
            }
        }
        )
    }


    private fun postrating() {


        try {

            if (NetWorkConection.isNEtworkConnected(this)) {


                //Set the Adapter to the RecyclerView//

                val apiServices = ApiClient.client.create(Api::class.java)
                val token = sharedPreferences.getString("token", "")
                val jid = sharedPreferences.getInt("jobid", 0)


                Log.e("idddddddd", "" + jid)

                Log.e("API : ", "" + apiServices)


                val call = apiServices.postrating("Token $token", jid, rate)

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<JobratingDefaultResponse> {
                    override fun onResponse(
                        call: Call<JobratingDefaultResponse>,
                        response: Response<JobratingDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


                        } else {
                            Toast.makeText(
                                this@Razorpay_Payment_activity,
                                "No Data",
                                Toast.LENGTH_LONG
                            ).show()


                        }
                    }

                    override fun onFailure(
                        call: Call<JobratingDefaultResponse>,
                        t: Throwable?
                    ) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }

    private fun createorder() {


        try {

            if (NetWorkConection.isNEtworkConnected(this)) {


                //Set the Adapter to the RecyclerView//

                val apiServices = ApiClient.client.create(Api::class.java)
                val token = sharedPreferences.getString("token", "")
                val id = sharedPreferences.getInt("bid", 0)



                Log.e("API : ", "" + apiServices)


                val call = apiServices.createorder("Token $token", id)

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<createorder_DefaultResponse> {
                    override fun onResponse(
                        call: Call<createorder_DefaultResponse>,
                        response: Response<createorder_DefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())




                        } else {
                            Toast.makeText(
                                this@Razorpay_Payment_activity,
                                "No Data",
                                Toast.LENGTH_LONG
                            ).show()


                        }
                    }

                    override fun onFailure(
                        call: Call<createorder_DefaultResponse>,
                        t: Throwable?
                    ) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }

    private fun startPayment() {
        /*
        *  You need to pass current activity in order to let Razorpay create CheckoutActivity
        * */
        val activity: Activity = this
        val co = Checkout()

        try {

            val options = JSONObject()
            options.put("name", "OY!")
            options.put("description", "Service Charges")
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("currency", "INR")
            options.put("amount", "100")
            options.put("send_sms_hash", true)

            val prefill = JSONObject()


            options.put("prefill", prefill)
            co.open(activity, options)

        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }


    }

//    override fun onPaymentSuccess(p0: String?, p1: PaymentData?) {
//
//
//        Toast.makeText(applicationContext, "sucess: " + razorpayPaymentId, Toast.LENGTH_LONG).show()
//
//        Log.e("paymentid", "" + )
//
//
//
//    }

    override fun onPaymentSuccess(p0: String?, p1: PaymentData?) {


        Log.e("signature",""+p1?.signature)
        Log.e("orderId",""+p1?.orderId)
        Log.e("paymentId",""+p1?.paymentId)

    }



    override fun onPaymentError(p0: Int, p1: String?, p2: PaymentData?) {

    }


}




