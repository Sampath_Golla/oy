package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class bidorder_Response(

    @SerializedName("id") val id : Int,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("bid_amount") val bid_amount : Int,
    @SerializedName("is_accepted") val is_accepted : Boolean,
    @SerializedName("message") val message : Int,
    @SerializedName("isPaid") val isPaid : Boolean,
    @SerializedName("job_id") val job_id : Int,
    @SerializedName("services") val services : Int
)