package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class CategoryListResponse(

    @SerializedName("id") val id : Int,
    @SerializedName("name") val name : String,
    @SerializedName("logo") val logo : String

)