package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class MyJobsdetaildefaultResponse(

    @SerializedName("status") val status : Boolean,
    @SerializedName("status_code") val status_code : Int,
    @SerializedName("code") val code : Int,
    @SerializedName("message") val message : String,
    @SerializedName("data") val data : MyJobsdetailListResponse
)