package com.supernal.oy.user.activity

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.supernal.oy.R
import com.supernal.oy.adapters.AutoCompleteAdapter
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.models.UserTypeDefaultResponse
import com.supernal.oy.utils.NetWorkConection
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class Oy_user_service_location : Activity() {
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var currentLocation: Location
    lateinit var geocoder: Geocoder
    var lng: Double = 0.0
    var lat: Double = 0.0
    private val permissionCode = 101
    lateinit var current_address: TextView
    lateinit var navigate_home: Button
    lateinit var token: String
    lateinit var Address: String
    lateinit var bussinessradiobutton: ImageView
    lateinit var homeradiobutton: ImageView
    lateinit var tap1: ConstraintLayout
    lateinit var tap2: ConstraintLayout
    lateinit var sharedPreferences: SharedPreferences
    lateinit var address_text: AutoCompleteTextView
    var placesClient: PlacesClient? = null
    var adapter: AutoCompleteAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_user_service_location)


        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fetchLocation()

        token = sharedPreferences.getString("token", "").toString()
        val loc = sharedPreferences.getString("location", "").toString()
        Log.e("usertoken", "" + token)
        navigate_home = findViewById(R.id.next_button)
        current_address = findViewById(R.id.current_address)
        bussinessradiobutton = findViewById(R.id.bussiness)
        homeradiobutton = findViewById(R.id.homeradio)
        address_text = findViewById(R.id.address_text)

        tap1 = findViewById(R.id.b)
        tap2 = findViewById(R.id.h)

        try {


            // Setup Places Client
            if (!Places.isInitialized()) {
//                Places.initialize(applicationContext, "AIzaSyAl_W-16U6yJI1RLf0gZdDd_IHoXFNi6L0")
                Places.initialize(applicationContext, "AIzaSyCWhZSGbO0WHgdWu35i2WMopQKKs2o0xQs")
            }

            placesClient = Places.createClient(this)
            address_text.threshold = 1
            address_text.onItemClickListener = autocompleteClickListener
            adapter = AutoCompleteAdapter(this, placesClient!!)
            address_text.setAdapter(adapter)

        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        tap1.setOnClickListener {

            bussinessradiobutton.visibility = View.VISIBLE
            homeradiobutton.visibility = View.GONE

        }
        tap2.setOnClickListener {

            bussinessradiobutton.visibility = View.GONE
            homeradiobutton.visibility = View.VISIBLE

        }

        navigate_home.setOnClickListener {
            patchuserservicetype()
        }




    }


    companion object

    fun fetchLocation() {
        if (ActivityCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                permissionCode
            )
            return
        }
        val task = fusedLocationProviderClient.lastLocation
        task.addOnSuccessListener { location ->
            if (location != null) {
                currentLocation = location
                lat = currentLocation.latitude.toDouble()
                lng = currentLocation.longitude.toDouble()

                val address = getAddress(lat, lng)

            }
        }
    }

    private fun getAddress(lat: Double, lng: Double): Any {
        val result = StringBuilder()
        val geocoder = Geocoder(this, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(lat, lng, 1)
            if (addresses.size > 0) {
                val address = addresses[0]
                result.append(address.subThoroughfare).append(",")
                result.append(address.thoroughfare).append(",")
                result.append(address.subLocality).append(",")
                result.append(address.locality).append(",")
                result.append(address.countryName)
                current_address.text = result
                Address = result.toString()
            }
        } catch (e: IOException) {
            e.message?.let { Log.e("tag", it) }
        }
        return result.toString()
    }

    private fun patchuserservicetype() {

        if (NetWorkConection.isNEtworkConnected(this)) {

            //Set the Adapter to the RecyclerView//

//            progressBarlogin.visibility = View.VISIBLE


            var apiServices = ApiClient.client.create(Api::class.java)

            val call =
                apiServices.patchusertype(
                    "Token $token",
                    "home",
                    lat.toString(),
                    lng.toString(), Address
                )
            Log.e("t", "" + token)
            Log.e("lat", "" + lat)
            Log.e("lng", "" + lng)


            val editor=sharedPreferences.edit()
            editor.putString("add",Address)
            editor.commit()

            call.enqueue(object : Callback<UserTypeDefaultResponse> {
                override fun onResponse(
                    call: Call<UserTypeDefaultResponse>,
                    response: Response<UserTypeDefaultResponse>

                ) {

//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, response.toString())

                    val status = response.body()?.status
                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        try {

                            val intent =
                                Intent(
                                    this@Oy_user_service_location,
                                    Oy_User::class.java
                                )
                            startActivity(intent)

                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }

                    } else {
                        Toast.makeText(
                            this@Oy_user_service_location,
                            "Invalid token ",
                            Toast.LENGTH_LONG
                        ).show()

                    }

                }


                override fun onFailure(call: Call<UserTypeDefaultResponse>, t: Throwable) {
//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, t.toString())
                    Toast.makeText(
                        this@Oy_user_service_location,
                        "Invalid ",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })


        } else {
            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }
    private val autocompleteClickListener =
        AdapterView.OnItemClickListener { adapterView, view, i, l ->
            try {
                val item: AutocompletePrediction = adapter?.getItem(i)!!
                var placeID: String? = null
                if (item != null) {
                    placeID = item.placeId
                }
                val placeFields: List<Place.Field> = Arrays.asList(
                    Place.Field.ID,
                    Place.Field.NAME,
                    Place.Field.ADDRESS,
                    Place.Field.LAT_LNG
                )
                var request: FetchPlaceRequest? = null
                if (placeID != null) {
                    request = FetchPlaceRequest.builder(placeID, placeFields)
                        .build()

                }
                if (request != null) {
                    placesClient?.fetchPlace(request)
                        ?.addOnSuccessListener(OnSuccessListener<Any> { task ->
                            Log.e("testcase", "" + task)

                        })?.addOnFailureListener(
                            OnFailureListener { e ->
                                e.printStackTrace()
                                address_text.setText(e.message)
                            })
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }



}
