package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class CancelListResponse(

    @SerializedName("id") val id : Int,
    @SerializedName("first_name") val first_name : String,
    @SerializedName("last_name") val last_name : String,
    @SerializedName("mobile") val mobile : String,
    @SerializedName("profile_pic") val profile_pic : String,
    @SerializedName("posted_on") val posted_on : String,
    @SerializedName("job_image") val job_image : String,
    @SerializedName("reason") val reason : Int,
    @SerializedName("comment") val comment : String
)