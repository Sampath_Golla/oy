package com.supernal.oy.service.ui.job_history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class JobHistoryViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is Job History Fragment"
    }
    val text: LiveData<String> = _text
}
