package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class JobratingListResponse(


    @SerializedName("id") val id : Int,
    @SerializedName("first_name") val first_name : String,
    @SerializedName("last_name") val last_name : String,
    @SerializedName("profile_pic") val profile_pic : String,
    @SerializedName("mobile") val mobile : String,
    @SerializedName("user_latitude") val user_latitude : Double,
    @SerializedName("user_longitude") val user_longitude : Double,
    @SerializedName("address") val address : String,
    @SerializedName("job_title") val job_title : String,
    @SerializedName("job_description") val job_description : String,
    @SerializedName("job_image") val job_image : String,
    @SerializedName("date_scheduled") val date_scheduled : String,
    @SerializedName("time_scheduled") val time_scheduled : String,
    @SerializedName("job_status") val job_status : String,
    @SerializedName("job_rating") val job_rating : Double,
    @SerializedName("posted_on") val posted_on : String
)