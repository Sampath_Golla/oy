package com.supernal.oy.service.activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.activity.Oy_LoginScreen
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.ActivityOyProviderBinding
import com.supernal.oy.models.service_profile_defaultResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Oy_Provider : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityOyProviderBinding
    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var profile_name: TextView
    lateinit var profileImage: ImageView
    lateinit var navView: NavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityOyProviderBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarOyProvider.toolbar)

        servicesharedPreferences =
            getSharedPreferences(
                "serviceloginprefs",
                Context.MODE_PRIVATE
            )

//        binding.appBarOyProvider.fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }
        val drawerLayout: DrawerLayout = binding.drawerLayout
         navView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_oy_provider)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_job_history,
                R.id.nav_profileinfo,
                R.id.nav_notifications,
                R.id.nav_settings,
                R.id.nav_switchmode,
                R.id.nav_logout
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        val headerView: View = binding.navView.getHeaderView(0)


        profile_name = headerView.findViewById(R.id.username)
        profileImage = headerView.findViewById(R.id.userImage)
        getusersProfile()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.oy_, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_oy_provider)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    fun onNavigationItemSelected(menuItem: MenuItem): Boolean {

        menuItem.isChecked = true

        val id = menuItem.itemId


        if (id == R.id.nav_logout) {

            logoutfun()
        }

        val drawer = binding.drawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
    fun logoutfun() {

        val editor = servicesharedPreferences.edit().clear().commit()

        intent = Intent(applicationContext, Oy_LoginScreen::class.java)
        startActivity(intent)
        finish()

    }

    private fun getusersProfile() {


        try {

            if (NetWorkConection.isNEtworkConnected(this)) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)

                var token = servicesharedPreferences.getString("servicetoken", "")
                Log.e("ptoken", "" + token)

                val call = apiServices.getservicerprofile("Token $token")

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<service_profile_defaultResponse> {
                    override fun onResponse(
                        call: Call<service_profile_defaultResponse>,
                        response: Response<service_profile_defaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


                            val pfname: String? =
                                response.body()!!.data.fullname
                            val pimage: String? = response.body()!!.data.profile_pic


                            profile_name.text = "$pfname"



                            Glide.with(applicationContext).load(pimage)
                                .apply(RequestOptions().centerCrop())
                                .error(R.drawable.electrician)
                                .into(profileImage)

//                            val editor = sharedPreferences.edit()
//                            editor.putString(
//                                "fname",
//                                response.body()!!.data.first_name + response.body()!!.data.last_name
//                            )
//                            editor.putString("gender", response.body()!!.data.gender)
//                            editor.putString("email", response.body()!!.data.email)
//                            editor.putString("mnumber", response.body()!!.data.mobile)
//                            editor.putString("loc", response.body()!!.data.address)
//                            editor.putString("pimage",response.body()!!.data.profile_pic)
//                            editor.commit()



                        } else {
                            Toast.makeText(this@Oy_Provider, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<service_profile_defaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }


}