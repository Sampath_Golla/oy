package com.supernal.oy.user.ui.servicestatus

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentUserBookingselectionBinding
import com.supernal.oy.models.CreatejobDefaultResponse
import com.supernal.oy.utils.NetWorkConection
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserBookingFragment : Fragment() {
    private var _binding: FragmentUserBookingselectionBinding? = null
    lateinit var sharedPreferences: SharedPreferences

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUserBookingselectionBinding.inflate(inflater, container, false)
        sharedPreferences = requireContext().getSharedPreferences(
            "loginprefs", Context.MODE_PRIVATE
        )
        val root: View = binding.root
        binding.scheduleNow.setOnClickListener {
            val navController =
                Navigation.findNavController(
                    context as Activity,
                    R.id.nav_host_fragment_content_oy_user
                )
            navController.navigate(R.id.nav_user_uploadService)
        }
        return root
    }




    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}