package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class UserTypeResponse(
    @SerializedName("user_type") val user_type : String,
    @SerializedName("latitude") val latitude : String,
    @SerializedName("longitude") val longitude : String,
    @SerializedName("address") val address : String

)