package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class BidamountListResponse(

    @SerializedName("job_id") val job_id : Int,
    @SerializedName("bid_amount") val bid_amount : Int,
    @SerializedName("message") val message : Int
)