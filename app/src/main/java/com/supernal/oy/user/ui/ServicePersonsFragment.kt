package com.supernal.oy.user.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentServicePersonsBinding
import com.supernal.oy.models.CategoryServicePersonsDefaultResponse
import com.supernal.oy.models.CategoryServicePersonsListResponse
import com.supernal.oy.models.CreatejobDefaultResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServicePersonsFragment : Fragment() {
    private var _binding: FragmentServicePersonsBinding? = null
    lateinit var servicepersonsadapter: Service_persons_adapter
    lateinit var servicepersonslist: List<CategoryServicePersonsListResponse>
    lateinit var sharedPreferences: SharedPreferences

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentServicePersonsBinding.inflate(inflater, container, false)
        sharedPreferences = requireContext().getSharedPreferences(
            "loginprefs", Context.MODE_PRIVATE
        )

        binding.servicePersonsSelection.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayout.VERTICAL, false)
        val root: View = binding.root

        binding.postservicepersons.setOnClickListener {



            postservicepeoplelist()

        }
        getservicepeoplelist()
        return root
    }

    private fun getservicepeoplelist() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = sharedPreferences.getString("token", "")
                var catid = sharedPreferences.getInt("category_id", 0)

                Log.e("API : ", "" + apiServices)


                val call = apiServices.Categoryservicepersonslist("Token $token", catid)

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<CategoryServicePersonsDefaultResponse> {
                    override fun onResponse(
                        call: Call<CategoryServicePersonsDefaultResponse>,
                        response: Response<CategoryServicePersonsDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())

                            servicepersonslist = response.body()?.data!!


                            servicepersonsadapter = Service_persons_adapter(
                                requireActivity(),
                                servicepersonslist
                            )
                            binding.servicePersonsSelection.adapter =
                                servicepersonsadapter
                            servicepersonsadapter.notifyDataSetChanged()


                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(
                        call: Call<CategoryServicePersonsDefaultResponse>,
                        t: Throwable?
                    ) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }


    private fun postservicepeoplelist() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = sharedPreferences.getString("token", "")
                var jobid = sharedPreferences.getInt("jobid", 0)

                Log.e("API : ", "" + apiServices)


                val call = apiServices.addservicepersons("Token $token", jobid, serviceidlist)

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<CreatejobDefaultResponse> {
                    override fun onResponse(
                        call: Call<CreatejobDefaultResponse>,
                        response: Response<CreatejobDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())
                            val navController =
                                Navigation.findNavController(
                                    context as Activity,
                                    R.id.nav_host_fragment_content_oy_user
                                )
                            navController.navigate(R.id.nav_user_home)

                            Toast.makeText(activity, "Service Created", Toast.LENGTH_LONG).show()


                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<CreatejobDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }

    companion object {
        var serviceidlist: MutableList<String> = ArrayList()


    }

    class Service_persons_adapter(
        var context: Context,
        var list: List<CategoryServicePersonsListResponse>

    ) :
        RecyclerView.Adapter<Service_persons_adapter.MyViewHolder>() {
        var personslist: MutableList<String> = ArrayList()

        lateinit var sharedPreferences: SharedPreferences
        lateinit var category_ist: List<CategoryServicePersonsListResponse>

        //inflating and returning our view holder

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

            sharedPreferences = context.getSharedPreferences("loginprefs", Context.MODE_PRIVATE)

            val view = LayoutInflater.from(context)
                .inflate(R.layout.service_person_adapter, parent, false)
            return MyViewHolder(view)
        }

        override fun getItemCount(): Int {


            return list.size


        }


        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


            holder.personname.text = list.get(position).fullname
            holder.servicerating.text = list.get(position).avg_stars


            var cat = list.get(position).category.map {

                val categoryname = it.name

                holder.profession.text = categoryname
            }


            var categoryimage = list.get(position).profile_pic

            Glide.with(context).load(categoryimage)
                .apply(RequestOptions().fitCenter())
                .error(R.drawable.electrician)
                .into(holder.profileimage)




            holder.servicecheckbox.setOnCheckedChangeListener { buttonView, isChecked ->


                if (isChecked) {

                    serviceidlist.add(list.get(position).id.toString())

                    var servicedpersonslist = LinkedHashSet(serviceidlist).toMutableList()


                    Log.e("servicedpersonslistejen", "" + servicedpersonslist)

                } else {

                    serviceidlist.remove(list.get(position).id.toString())


                }
            }


        }


        class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {


            val profileimage = itemView!!.findViewById(R.id.profilepic) as ImageView
            val personname = itemView!!.findViewById(R.id.name) as TextView
            val profession = itemView!!.findViewById(R.id.profession) as TextView
            val servicerating = itemView!!.findViewById(R.id.servicerating) as TextView
            val servicecheckbox = itemView!!.findViewById(R.id.checkbox) as CheckBox


        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}