package com.supernal.oy.api

import com.supernal.oy.models.*
import retrofit2.Call
import retrofit2.http.*

interface Api {

// User APis
//Login tyoe-mobile number

    @FormUrlEncoded
    @POST("core/sign-up/")
    fun postloginumnber(
        @Field("mobile") mobile: String,
        @Field("type") type: String
    ): Call<LoginDefaultResponse>


    //Login type-mobile number
    @FormUrlEncoded
    @POST("core/login/")
    fun postgooglelogin(
        @Field("type") type: String,
        @Field("login_type") login_type: String,
        @Field("email") email: String
    ): Call<LoginDefaultResponse>

    //Patch user service location data
    @FormUrlEncoded
    @PATCH("users/user_type/")
    fun patchusertype(
        @Header("Authorization") authorization: String,
        @Field("user_type") user_type: String,
        @Field("latitude") latitude: String,
        @Field("longitude") longitude: String,
        @Field("address") address: String
    ): Call<UserTypeDefaultResponse>


    //Get user profile
    @GET("users/user-profile/me")
    fun getuserprofile(
        @Header("Authorization") authorization: String
    ): Call<ProfileDefaultResponse>

    //Get service profile
    @GET("services/service-profile/me")
    fun getservicerprofile(
        @Header("Authorization") authorization: String
    ): Call<service_profile_defaultResponse>

    //Get Categories
    @GET("services/category/")
    fun getcategories(
    ): Call<CategoryDefaultResponse>

    //Create job
    @FormUrlEncoded
    @POST("job/job_create/")
    fun Createjob(
        @Header("Authorization") authorization: String,
        @Field("job_title") job_title: String,
        @Field("job_description") job_description: String,
        @Field("job_image") job_image: String,
        @Field("date_scheduled") date_scheduled: String,
        @Field("time_scheduled") time_scheduled: String
    ): Call<CreatejobDefaultResponse>

    //Category based service persons
    @FormUrlEncoded
    @POST("users/service_list/category_id/")
    fun Categoryservicepersonslist(
        @Header("Authorization") authorization: String,
        @Field("id") id: Int,
    ): Call<CategoryServicePersonsDefaultResponse>


    //selected service persons
    @FormUrlEncoded
    @POST("job/job_update_service/")
    fun addservicepersons(
        @Header("Authorization") authorization: String,
        @Field("id") id: Int,
        @Field("service") service: MutableList<String>,
    ): Call<CreatejobDefaultResponse>


//    Service APis


    //Rejister a service person
    @FormUrlEncoded
    @PATCH("services/update-services/")
    fun rejisterserviceperson(
        @Header("Authorization") authorization: String,
        @Field("fullname") fullname: String,
        @Field("mobile") mobile: String,
        @Field("email") email: String,
        @Field("profile_pic") profile_pic: String,
        @Field("addhaar_number") addhaar_number: String,
        @Field("addhaar_front_image") addhaar_front_image: String,
        @Field("addhaar_back_image") addhaar_back_image: String,
        @Field("category") category: String,
        @Field("experience") experience: String,
    ): Call<RejistrationDefaultResponse>


    //Get user verification
    @GET("services/is_verified")
    fun getuserverificationstatus(
        @Header("Authorization") authorization: String
    ): Call<VerificationDefaultResponse>


    //Leads info
    @FormUrlEncoded
    @POST("job/leads/select_date/")
    fun leadslist(
        @Header("Authorization") authorization: String,
        @Field("date") date: String
    ): Call<LeadsDefaultResponse>

    //Leads detail info
    @FormUrlEncoded
    @POST("job/leads/detail/")
    fun leadsdetailist(
        @Header("Authorization") authorization: String,
        @Field("id") id: Int
    ): Call<BidDefaultResponse>

    //post bid amount
    @FormUrlEncoded
    @POST("job/bid_now/")
    fun postbidamount(
        @Header("Authorization") authorization: String,
        @Field("job_id") job_id: Int,
        @Field("bid_amount") bid_amount: String
    ): Call<BidamountDefaultResponse>


    //Get user bid detail
    @GET("job/bid-status/get")
    fun getuserbids(
        @Header("Authorization") authorization: String
    ): Call<userbiddetailDefaultResponse>

    //Get serviceperson details
    @FormUrlEncoded
    @POST("job/bid-status/details/")
    fun getservicepopup(
        @Header("Authorization") authorization: String,
        @Field("id") id: String
    ): Call<userbiddetailPopupResponse>

    //confirm service
    @FormUrlEncoded
    @POST("job/confirm-bid/details/")
    fun confirmservice(
        @Header("Authorization") authorization: String,
        @Field("id") id: String
    ): Call<userbiddetailPopupResponse>


    //Get user services confirmed status
    @GET("job/corfirmed_bid_list/")
    fun getuserservicestatus(
        @Header("Authorization") authorization: String
    ): Call<userbiddetailDefaultResponse>


    //Get cancel reasons
    @GET("job/reason_list/")
    fun getreasonslist(
    ): Call<Cancel_Reason_DefaultResponse>

    //Getservice persons jobs list
    @FormUrlEncoded
    @POST("job/my-jobs/select_status/")
    fun getservicepersonsjobslist(
        @Header("Authorization") authorization: String,
        @Field("job_status") job_status: String
    ): Call<MyJobsDefaultResponse>

    //indetail job api
    @FormUrlEncoded
    @POST("job/my-jobs/detail/")
    fun jobindetailresponse(
        @Header("Authorization") authorization: String,
        @Field("id") id: Int
    ): Call<MyJobsdetaildefaultResponse>

    //post job otp
    @FormUrlEncoded
    @POST("job/job_otp_verify/")
    fun postjobotp(
        @Header("Authorization") authorization: String,
        @Field("job_otp") job_otp: String,
        @Field("id") id: Int
    ): Call<MyJobsdetaildefaultResponse>


    //jobdone
    @FormUrlEncoded
    @POST("job/job_status/")
    fun postjobdone(
        @Header("Authorization") authorization: String,
        @Field("id") id: Int
    ): Call<MyJobsdetaildefaultResponse>


    //job history
    @FormUrlEncoded
    @POST("job/job-history/get/")
    fun getjobhistory(
        @Header("Authorization") authorization: String,
        @Field("job_status") job_status: String
    ): Call<HistoryDefaultResponse>


    //patch profile
    @FormUrlEncoded
    @PATCH("users/user-profile-update/")
    fun patchprofile(
        @Header("Authorization") authorization: String,
        @Field("first_name") fullname: String,
        @Field("mobile") mobile: String,
        @Field("email") email: String,
        @Field("profile_pic") profile_pic: String,
        @Field("gender") gender: String,
        @Field("address") address: String
    ): Call<PatchProfileDefaultResponse>


    //post rating
    @FormUrlEncoded
    @POST("job/user_rate_job/")
    fun postrating(
        @Header("Authorization") authorization: String,
        @Field("id") id: Int,
        @Field("job_rating") job_rating: String
    ): Call<JobratingDefaultResponse>


    //cancel jb
    @FormUrlEncoded
    @POST("job/cancel_job/")
    fun canceljob(
        @Header("Authorization") authorization: String,
        @Field("job") job: Int,
        @Field("reason") reason: String,
        @Field("comment") comment: String
    ): Call<CancelDefaultResponse>


    //    payment APi's



    //create order
    @FormUrlEncoded
    @POST("users/pay/")
    fun createorder(
        @Header("Authorization") authorization: String,
        @Field("id") id: Int
    ): Call<createorder_DefaultResponse>


}