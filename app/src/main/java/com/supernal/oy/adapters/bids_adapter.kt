package com.supernal.oy.adapters

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.models.CategoryListResponse
import com.supernal.oy.models.userbiddetailListResponse


class bids_adapter(var context: Context, var list: List<userbiddetailListResponse>) :
    RecyclerView.Adapter<bids_adapter.MyViewHolder>() {

    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var category_ist: List<CategoryListResponse>

    //inflating and returning our view holder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        servicesharedPreferences = context.getSharedPreferences("serviceloginprefs", Context.MODE_PRIVATE)

        val view = LayoutInflater.from(context)
            .inflate(R.layout.fragment_user_notifications_adapter, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {


        return list.size


    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


       val personname = list.get(position).services.fullname

        holder.name.text=personname

        val profname = list.get(position).services.category.map {

            val profname=it.name

            holder.profession.text = profname

        }

        val bidamount = list.get(position).bid_amount

        holder.bidcost.text= bidamount


        var profileimage = list.get(position).services.profile_pic

        Glide.with(context).load(profileimage)
            .apply(RequestOptions().fitCenter())
            .error(R.drawable.electrician)
            .into(holder.servicepersonprofile)
    }


    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {


        val servicepersonprofile = itemView!!.findViewById(R.id.servicepersonprofile) as ImageView
        val name = itemView!!.findViewById(R.id.name) as TextView
        val profession = itemView!!.findViewById(R.id.profession) as TextView
        val bidcost = itemView!!.findViewById(R.id.bidcost) as TextView


    }
}