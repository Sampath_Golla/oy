package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class UserTypeDefaultResponse(

    @SerializedName("status") val status : Boolean,
    @SerializedName("code") val code : Int,
    @SerializedName("Message") val message : String,
    @SerializedName("data") val data : UserTypeResponse
)