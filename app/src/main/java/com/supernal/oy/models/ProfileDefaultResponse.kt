package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class ProfileDefaultResponse(
    @SerializedName("status") val status : Boolean,
    @SerializedName("code") val code : Int,
    @SerializedName("message") val message : String,
    @SerializedName("data") val data : ProfileResponse
)