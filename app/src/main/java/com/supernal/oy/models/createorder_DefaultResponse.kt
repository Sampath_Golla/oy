package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class createorder_DefaultResponse(

    @SerializedName("status") val status : Boolean,
    @SerializedName("status_code") val status_code : Int,
    @SerializedName("code") val code : Int,
    @SerializedName("message") val message : String,
    @SerializedName("payment") val payment : Payment_Response,
    @SerializedName("order") val order : order_Response
)