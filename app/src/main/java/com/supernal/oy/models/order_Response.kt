package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class order_Response(

    @SerializedName("id") val id : Int,
    @SerializedName("order_date") val order_date : String,
    @SerializedName("created_at") val created_at : String,
    @SerializedName("updated_at") val updated_at : String,
    @SerializedName("order_amount") val order_amount : Int,
    @SerializedName("order_payment_id") val order_payment_id : String,
    @SerializedName("isPaid") val isPaid : Boolean,
    @SerializedName("bidjob") val bidjob : bidorder_Response
)