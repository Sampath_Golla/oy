package com.supernal.oy.user.ui.servicestatus

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.format.DateFormat
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.*
import com.supernal.oy.models.CreatejobDefaultResponse
import com.supernal.oy.models.ProfileDefaultResponse
import com.supernal.oy.utils.ManagePermissions
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Logger


class UserUploadServiceFragment : Fragment(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {
    private var _binding: FragmentUserUploadServiceBinding? = null
    lateinit var sharedPreferences: SharedPreferences
    lateinit var date: String
    lateinit var time: String
    lateinit var jobdiscription: String
    private val STORAGE_REQUEST_CODE = 101
    private val CAMERA_REQUEST_CODE = 123
    private val PermissionsRequestCode = 123
    private var camerapath: String? = null
    private var picturePath: String? = null
    private var fileUri: Uri? = null
    private var imageString: String? = null
    private lateinit var managePermissions: ManagePermissions
    private var mImageFileLocation = ""

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var datepicker_btn: Button
    lateinit var timepicker_btn: Button
    lateinit var calender: Calendar
    lateinit var uploadImage: ImageView
    val REQUEST_CODE = 200
    var day = 0
    var hour = 0
    var minute = 0
    var month: Int = 0
    var year: Int = 0
    var myDay = 0
    var myMonth: Int = 0
    var myYear: Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUserUploadServiceBinding.inflate(inflater, container, false)

        sharedPreferences = requireContext().getSharedPreferences(
            "loginprefs", Context.MODE_PRIVATE
        )
        val root: View = binding.root
        datepicker_btn = binding.dateBtn
        timepicker_btn = binding.timeBtn
        calender = Calendar.getInstance()
        val df = SimpleDateFormat("dd-MM-yyyy")
        val formattedDate = df.format(calender.getTime())
        val currentTime = SimpleDateFormat("hh:mm aa")
        val formattedTime = currentTime.format(calender.getTime())
        timepicker_btn.text = formattedTime
        datepicker_btn.text = formattedDate


        datepicker_btn.setOnClickListener {
            val calender: Calendar = Calendar.getInstance()
            day = calender.get(Calendar.DAY_OF_MONTH)
            month = calender.get(Calendar.MONTH)
            year = calender.get(Calendar.YEAR)
            val datePickerDialog = DatePickerDialog(
                context as Activity,
                this@UserUploadServiceFragment,
                year,
                month,
                day
            )
            datePickerDialog.datePicker.setMinDate(System.currentTimeMillis() - 1000)
            datePickerDialog.show()
        }

        val list = listOf<String>(
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CALENDAR
        )


        managePermissions =
            ManagePermissions(requireContext() as Activity, list, PermissionsRequestCode)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            managePermissions.checkPermissions()


        binding.uploadImg.setOnClickListener {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                managePermissions.checkPermissions()
            selectImage()

        }

        timepicker_btn.setOnClickListener {
            hour = calender.get(Calendar.HOUR_OF_DAY)
            minute = calender.get(Calendar.MINUTE)

            val timePickerDialog = TimePickerDialog(
                context as Activity, this@UserUploadServiceFragment, hour, minute,
                DateFormat.is24HourFormat(context as Activity)
            )
            timePickerDialog.show()
        }


        binding.postjobdetails.setOnClickListener {

            jobdiscription = binding.serviceDesc.text.toString().trim()

            getusersProfile()
        }
        return root
    }


    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        myDay = dayOfMonth
        myYear = year
        myMonth = month + 1
        val calender: Calendar = Calendar.getInstance()
        calender.set(Calendar.MONTH, month)
        datepicker_btn.text = "" + myYear + "-" + myMonth + "-" + myDay
        date = datepicker_btn.text.toString().trim()

    }


    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {

        val c = Calendar.getInstance()
        calender.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calender.set(Calendar.MINUTE, minute);

        if (calender.getTimeInMillis() >= c.getTimeInMillis() + (60 * 60 * 1000)) {
            //it's after current
            val hour: Int = hourOfDay % 12
            timepicker_btn.setText(
                String.format(
                    "%02d:%02d %s", if (hour == 0) 12 else hour,
                    minute, if (hourOfDay < 12) "am" else "pm"
                )
            )
            time = timepicker_btn.text.toString()
        } else {
            //it's before current'
            Toast.makeText(context as Activity, "Invalid Time", Toast.LENGTH_LONG).show()
        }
    }


    private fun selectImage() {
        try {

            val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Add Photo!")
            builder.setItems(options) { dialog, item ->
                if (options[item] == "Take Photo") {


                    val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(intent, CAMERA_REQUEST_CODE)


                } else if (options[item] == "Choose from Gallery") {
                    val intent =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    startActivityForResult(intent, STORAGE_REQUEST_CODE)
                } else if (options[item] == "Cancel") {
                    dialog.dismiss()
                }
            }
            builder.show()
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (data != null) {


                if (resultCode == Activity.RESULT_OK) {
                    if (requestCode == CAMERA_REQUEST_CODE) {
                        val extras = data.extras
                        val imageBitmap = extras?.get("data") as Bitmap
                        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmSS").format(Date())
                        val imageFileName = timeStamp
                        // Here we specify the environment location and the exact path where we want                       to save the so-created file
                        val storageDirectory =
                            (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS))
                        Logger.getAnonymousLogger().info("Storage directory set")


                        val image = File.createTempFile(imageFileName, ".jpg", storageDirectory)

                        // Here the location is saved into the string mImageFileLocation
                        Logger.getAnonymousLogger().info("File name and path set")

                        var out: FileOutputStream? = null
                        try {
                            out = FileOutputStream(image)
                            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
                            out.flush()
                            out.close()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        camerapath = image.absolutePath
                        fileUri = Uri.parse(mImageFileLocation)
                        Log.e("camerauri", "" + fileUri)

                        binding.uploadedImage.setImageBitmap(imageBitmap)


                        imageString = Base64.encodeToString(
                            getBytesFromBitmap(imageBitmap),
                            Base64.NO_WRAP
                        )


                        Log.e("b64", "" + imageString)

                        Log.e("imagename", "" + imageString)

//
                        // userImageUpdate(imageString)
                    } else if (requestCode == STORAGE_REQUEST_CODE) {
                        if (data != null) {
                            // Get the Image from data
                            val selectedImage = data.data
                            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                            val cursor = requireContext().contentResolver.query(
                                selectedImage!!,
                                filePathColumn,
                                null,
                                null,
                                null
                            )
                            assert(cursor != null)
                            cursor!!.moveToFirst()

                            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                            picturePath = cursor.getString(columnIndex)
                            // Set the Image in ImageView for Previewing the Media
                            var imageBitmap = BitmapFactory.decodeFile(picturePath)
                            binding.uploadedImage.setImageBitmap(imageBitmap)
                            cursor.close()

                            Log.e("galleryPath", "" + picturePath)


                            imageString = Base64.encodeToString(
                                getBytesFromBitmap(imageBitmap),
                                Base64.NO_WRAP
                            )
                            Log.e("b64", "" + imageString)

                        }

                    }


                }


            } else {
                Toast.makeText(activity as Activity, "Select Shop Image...", Toast.LENGTH_LONG)
                    .show()

            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }


    fun getBytesFromBitmap(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream)
        return stream.toByteArray()
    }


    private fun postjobdetails() {

        if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {

            //Set the Adapter to the RecyclerView//

//            progressBarlogin.visibility = View.VISIBLE


            var apiServices = ApiClient.client.create(Api::class.java)


            var token = sharedPreferences.getString("token", "")
            var categoryname = sharedPreferences.getString("category_name", "")
            Log.e("ptoken", "" + token)

            val call =
                imageString?.let {
                    apiServices.Createjob(
                        "Token $token",
                        categoryname.toString(),
                        jobdiscription,
                        "data:image/png;base64," + imageString!!,
                        date,
                        time
                    )
                }



            if (call != null) {
                call.enqueue(object : Callback<CreatejobDefaultResponse> {
                    override fun onResponse(
                        call: Call<CreatejobDefaultResponse>,
                        response: Response<CreatejobDefaultResponse>

                    ) {

                        //                    progressBarlogin.visibility = View.GONE
                        Log.e(ContentValues.TAG, response.toString())

                        val status = response.body()?.status
                        if (response.isSuccessful) {

                            //Set the Adapter to the RecyclerView//

                            try {

                                val navController = Navigation.findNavController(
                                    activity!!,
                                    R.id.nav_host_fragment_content_oy_user
                                )
                                navController.navigate(R.id.nav_user_servicePersons)
                                val editor = sharedPreferences.edit()
                                editor.putInt("jobid", response.body()?.data!!.id)
                                editor.commit()

                                //postOtp()
                            } catch (e: NullPointerException) {
                                e.printStackTrace()
                            } catch (e: TypeCastException) {
                                e.printStackTrace()
                            }

                        } else {
                            Toast.makeText(
                                activity,
                                "Invalid token ",
                                Toast.LENGTH_LONG
                            ).show()

                        }

                    }


                    override fun onFailure(call: Call<CreatejobDefaultResponse>, t: Throwable) {
                        //                    progressBarlogin.visibility = View.GONE
                        Log.e(ContentValues.TAG, t.toString())
                        Toast.makeText(
                            activity,
                            "Invalid ",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                })
            }


        } else {
            Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }


    private fun getusersProfile() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)

                var token = sharedPreferences.getString("token", "")
                Log.e("ptoken", "" + token)

                val call = apiServices.getuserprofile("Token $token")

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<ProfileDefaultResponse> {
                    override fun onResponse(
                        call: Call<ProfileDefaultResponse>,
                        response: Response<ProfileDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


                            val pfname: String? =
                                response.body()!!.data.first_name + response.body()!!.data.last_name
                            val pflast: String? = response.body()!!.data.last_name
                            val pgender: String? = response.body()!!.data.gender
                            val pemail: String? = response.body()!!.data.email
                            val pmobile: String? = response.body()!!.data.mobile
                            val paddress: String? = response.body()!!.data.address
                            val pimage: String? = response.body()!!.data.profile_pic


                            if (response.body()!!.data.first_name == "") {


                                Toast.makeText(activity, "Please Update Profile", Toast.LENGTH_LONG)
                                    .show()


                            } else {
                                postjobdetails()

                            }


                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<ProfileDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}