package com.supernal.oy.activity

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.supernal.oy.user.activity.Oy_user_service_location
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.models.LoginDefaultResponse
import com.supernal.oy.service.activity.Oy_Provider
import com.supernal.oy.utils.NetWorkConection
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Oy_SelectionScreen : Activity() {
    lateinit var providerBtn: Button
    lateinit var userBtn: Button
    lateinit var provider_btn: Button
    lateinit var user_mobile: String
    lateinit var user_email: String
    lateinit var sharedPreferences: SharedPreferences
    lateinit var servicesharedPreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_selection_screen)
        providerBtn = findViewById(R.id.provider_btn)

        sharedPreferences =
            getSharedPreferences(
                "loginprefs",
                Context.MODE_PRIVATE
            )

        servicesharedPreferences =
            getSharedPreferences(
                "serviceloginprefs",
                Context.MODE_PRIVATE
            )

        user_mobile = sharedPreferences.getString("usermobilenumber", "").toString()
        user_email = sharedPreferences.getString("useremail", "").toString()




        userBtn = findViewById(R.id.user_btn)
        provider_btn = findViewById(R.id.provider_btn)

        userBtn.setOnClickListener {


            postusermobilenumber()
//            postusergoogleemail()
        }

        provider_btn.setOnClickListener {
            postserviceusermobilenumber()

        }
    }


    private fun postusermobilenumber() {

        if (NetWorkConection.isNEtworkConnected(this)) {
            //Set the Adapter to the RecyclerView//

//        progressBarlogin.visibility = View.VISIBLE
            var apiServices = ApiClient.client.create(Api::class.java)

            val call =
                apiServices.postloginumnber(user_mobile, "users")

            call.enqueue(object : Callback<LoginDefaultResponse> {
                override fun onResponse(
                    call: Call<LoginDefaultResponse>,
                    response: Response<LoginDefaultResponse>

                ) {

//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, response.toString())

                    val status = response.body()?.status
                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        try {


                            val intent =
                                Intent(
                                    this@Oy_SelectionScreen,
                                    Oy_user_service_location::class.java
                                )
                            startActivity(intent)

                            val editor = sharedPreferences.edit()
                            editor.putString("token", response.body()?.data!!.token)
                            editor.putBoolean("islogined", true)
                            Log.e("mtoken", "" + response.body()?.data!!.token)
                            editor.commit()

                            //postOtp()
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }

                    } else {
                        Toast.makeText(
                            this@Oy_SelectionScreen,
                            "Invalid Mobile Number",
                            Toast.LENGTH_LONG
                        ).show()

                    }

                }


                override fun onFailure(call: Call<LoginDefaultResponse>, t: Throwable) {
//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, t.toString())
                    Toast.makeText(
                        this@Oy_SelectionScreen,
                        "Invalid Mobile Number",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })


        } else {
            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }

    private fun postserviceusermobilenumber() {

        if (NetWorkConection.isNEtworkConnected(this)) {

            //Set the Adapter to the RecyclerView//

//            progressBarlogin.visibility = View.VISIBLE


            var apiServices = ApiClient.client.create(Api::class.java)

            val call =
                apiServices.postloginumnber(user_mobile, "services")

            call.enqueue(object : Callback<LoginDefaultResponse> {
                override fun onResponse(
                    call: Call<LoginDefaultResponse>,
                    response: Response<LoginDefaultResponse>

                ) {

//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, response.toString())

                    val status = response.body()?.status
                    if (response.isSuccessful) {


                        if (response.code().equals(11)) {
                            try {


                                val intent =
                                    Intent(
                                        this@Oy_SelectionScreen,
                                        Oy_Registration::class.java
                                    )
                                startActivity(intent)

                                val editor = servicesharedPreferences.edit()
                                editor.putString("servicetoken", response.body()?.data!!.token.toString())
                                editor.putBoolean("islogin", true)
                                Log.e("mtoken", "" + response.body()?.data!!.token.toString())
                                editor.commit()

                                //postOtp()
                            } catch (e: NullPointerException) {
                                e.printStackTrace()
                            } catch (e: TypeCastException) {
                                e.printStackTrace()
                            }


                        } else {

                            startActivity(Intent(this@Oy_SelectionScreen, Oy_Provider::class.java))
                            val editor = servicesharedPreferences.edit()
                            editor.putBoolean("islogin", true)
                            editor.putString("servicetoken", response.body()?.data!!.token.toString())
                            Log.e("mtoken", "" + response.body()?.data!!.token.toString())
                            editor.commit()


                        }


                    } else {
                        Toast.makeText(
                            this@Oy_SelectionScreen,
                            "Invalid Mobile Number",
                            Toast.LENGTH_LONG
                        ).show()

                    }

                }


                override fun onFailure(call: Call<LoginDefaultResponse>, t: Throwable) {
//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, t.toString())
                    Toast.makeText(
                        this@Oy_SelectionScreen,
                        "Invalid Mobile Number",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })


        } else {
            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }


    private fun postusergoogleemail() {

        if (NetWorkConection.isNEtworkConnected(this)) {

            //Set the Adapter to the RecyclerView//

//            progressBarlogin.visibility = View.VISIBLE


            var apiServices = ApiClient.client.create(Api::class.java)

            val call =
                apiServices.postgooglelogin("users", "google", user_email)

            call.enqueue(object : Callback<LoginDefaultResponse> {
                override fun onResponse(
                    call: Call<LoginDefaultResponse>,
                    response: Response<LoginDefaultResponse>

                ) {

//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, response.toString())

                    val status = response.body()?.status
                    if (response.isSuccessful) {

                        //Set the Adapter to the RecyclerView//

                        try {


                            val intent =
                                Intent(
                                    this@Oy_SelectionScreen,
                                    Oy_user_service_location::class.java
                                )
                            startActivity(intent)

                            val editor = sharedPreferences.edit()
                            editor.putString("token", response.body()?.data!!.token.toString())
                            editor.commit()

                            //postOtp()
                        } catch (e: NullPointerException) {
                            e.printStackTrace()
                        } catch (e: TypeCastException) {
                            e.printStackTrace()
                        }

                    } else {
                        Toast.makeText(
                            this@Oy_SelectionScreen,
                            "Invalid  Email",
                            Toast.LENGTH_LONG
                        ).show()

                    }

                }


                override fun onFailure(call: Call<LoginDefaultResponse>, t: Throwable) {
//                    progressBarlogin.visibility = View.GONE
                    Log.e(ContentValues.TAG, t.toString())
                    Toast.makeText(
                        this@Oy_SelectionScreen,
                        "Invalid Email",
                        Toast.LENGTH_LONG
                    ).show()
                }
            })


        } else {
            Toast.makeText(this, "Please Check your internet", Toast.LENGTH_LONG).show()

        }

    }


}