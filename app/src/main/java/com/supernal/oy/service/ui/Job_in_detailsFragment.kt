package com.supernal.oy.service.ui

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentMyjobdetailsBinding
import com.supernal.oy.models.MyJobsdetaildefaultResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Job_in_detailsFragment : Fragment() {

    private var _binding: FragmentMyjobdetailsBinding? = null
    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var otpdigit: String
    lateinit var otpgiven: EditText
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMyjobdetailsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        servicesharedPreferences = requireContext().getSharedPreferences(
            "serviceloginprefs", Context.MODE_PRIVATE
        )


        getjobinfo()



        binding.startjob.setOnClickListener {


            val dialog = Dialog(requireContext())
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(true)
            dialog.setContentView(R.layout.job_start_otp)
            dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            val cancel =
                dialog.findViewById(R.id.cancelbutton) as ImageView
            val confirm =
                dialog.findViewById(R.id.confirmorder) as Button
            otpgiven =
                dialog.findViewById(R.id.job_id_otp) as EditText


            confirm.setOnClickListener {
                otpdigit = otpgiven.text.toString().trim()
                Log.e("otpss", "" + otpdigit)

                postotp(otpdigit)
                dialog.dismiss()
            }
            cancel.setOnClickListener { dialog.dismiss() }
            dialog.show()

        }
        binding.jobdone.setOnClickListener {

            postjobdone()
        }


        binding.canceljob.setOnClickListener {

            val navController = Navigation.findNavController(
                requireActivity(),
                R.id.nav_host_fragment_content_oy_provider
            )
            navController.navigate(R.id.nav_cancel_jobs)

        }
        return root
    }

    private fun getjobinfo() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = servicesharedPreferences.getString("servicetoken", "")
                var jid = servicesharedPreferences.getInt("jobid", 0)

                Log.e("API : ", "" + apiServices)


//                val call = apiServices.leadslist("Token $token")
//                val call =
//                    apiServices.jobindetailresponse(
//                        "Token $token",
//                        jid

                val call =
                    apiServices.jobindetailresponse(
                        "Token $token", jid
                    )

                Log.e("API Categories : ", "" + call)

                call.enqueue(object : Callback<MyJobsdetaildefaultResponse> {
                    override fun onResponse(
                        call: Call<MyJobsdetaildefaultResponse>,
                        response: Response<MyJobsdetaildefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


                            if (response.body()!!.data.job_status == "Pending") {

                                binding.startjob.visibility = View.VISIBLE
                                binding.canceljob.visibility = View.VISIBLE
                                binding.jobdone.visibility = View.GONE
                            } else if (response.body()!!.data.job_status == "InProgress") {
                                binding.startjob.visibility = View.GONE
                                binding.jobdone.visibility = View.VISIBLE
                                binding.canceljob.visibility = View.GONE
                                binding.note.visibility = View.VISIBLE

                            }
                            binding.providerName.text = response.body()!!.data.first_name
                            binding.posteddatetime.text =
                                " Posted on  " + response.body()!!.data.posted_on

                            binding.detailDesc.text = response.body()!!.data.job_description
                            binding.timings.text =
                                response.body()!!.data.date_scheduled + " , " + response.body()!!.data.time_scheduled
                            binding.locationDetails.text = response.body()!!.data.address

                            var serviceimage = response.body()!!.data.job_image
                            var profilepic = response.body()!!.data.profile_pic

                            context?.let {
                                Glide.with(it).load(profilepic)
                                    .apply(RequestOptions().fitCenter())
                                    .error(R.drawable.electrician)
                                    .into(binding.profileImg)
                            }

                            context?.let {
                                Glide.with(it).load(serviceimage)
                                    .apply(RequestOptions().fitCenter())
                                    .error(R.drawable.electrician)
                                    .into(binding.serviceimageview)
                            }

                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<MyJobsdetaildefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }

    private fun postotp(otp: String) {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = servicesharedPreferences.getString("servicetoken", "")
                var jid = servicesharedPreferences.getInt("jobid", 0)

                Log.e("API : ", "" + apiServices)
                Log.e("APIotp : ", "" + otp)


//                val call = apiServices.leadslist("Token $token")
//                val call =
//                    apiServices.jobindetailresponse(
//                        "Token $token",
//                        jid

                val call =
                    apiServices.postjobotp(
                        "Token $token", otp, jid
                    )

                Log.e("API Categories : ", "" + call)

                Log.e("otp", "" + otpdigit)
                call.enqueue(object : Callback<MyJobsdetaildefaultResponse> {
                    override fun onResponse(
                        call: Call<MyJobsdetaildefaultResponse>,
                        response: Response<MyJobsdetaildefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())
                            Log.e("responses", "" + response)

                            getjobinfo()

                        } else {
                            Toast.makeText(activity, "Invalid OTP" , Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<MyJobsdetaildefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }
    private fun postjobdone() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = servicesharedPreferences.getString("servicetoken", "")
                var jid = servicesharedPreferences.getInt("jobid", 0)

                Log.e("API : ", "" + apiServices)


//                val call = apiServices.leadslist("Token $token")
//                val call =
//                    apiServices.jobindetailresponse(
//                        "Token $token",
//                        jid

                val call =
                    apiServices.postjobdone(
                        "Token $token",  jid
                    )

                Log.e("API Categories : ", "" + call)

                call.enqueue(object : Callback<MyJobsdetaildefaultResponse> {
                    override fun onResponse(
                        call: Call<MyJobsdetaildefaultResponse>,
                        response: Response<MyJobsdetaildefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())
                            Log.e("responses", "" + response)
                            val navController = Navigation.findNavController(
                                activity!!,
                                R.id.nav_host_fragment_content_oy_provider
                            )
                            navController.navigate(R.id.nav_home)

                        } else {
                            Toast.makeText(activity, ""+ response.body()!!.message , Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<MyJobsdetaildefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}