package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class CreatejobListResponse(

    @SerializedName("id") val id : Int,
    @SerializedName("job_title") val job_title : String,
    @SerializedName("job_description") val job_description : String,
    @SerializedName("job_image") val job_image : String,
    @SerializedName("date_scheduled") val date_scheduled : String,
    @SerializedName("time_scheduled") val time_scheduled : String,
    @SerializedName("job_status") val job_status : String,
    @SerializedName("job_rating") val job_rating : String,
    @SerializedName("service") val service : List<String>
)