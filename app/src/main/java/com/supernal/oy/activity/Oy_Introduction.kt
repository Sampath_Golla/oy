package com.supernal.oy.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import com.supernal.oy.R

class Oy_Introduction:Activity() {
    lateinit var getStarted: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_introduction)
        getStarted=findViewById(R.id.getStarted)
        getStarted.setOnClickListener{
            val intent= Intent(this@Oy_Introduction, Oy_LoginScreen::class.java)
            startActivity(intent)
        }
    }
}