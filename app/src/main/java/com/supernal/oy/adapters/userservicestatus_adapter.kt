package com.supernal.oy.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.payment.Razorpay_Payment_activity
import com.supernal.oy.models.CategoryListResponse
import com.supernal.oy.models.userbiddetailListResponse


class userservicestatus_adapter(var context: Context, var list: List<userbiddetailListResponse>) :
    RecyclerView.Adapter<userservicestatus_adapter.MyViewHolder>() {

    lateinit var sharedPreferences: SharedPreferences
    lateinit var category_ist: List<CategoryListResponse>
    //inflating and returning our view holder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        sharedPreferences = context.getSharedPreferences("loginprefs", Context.MODE_PRIVATE)

        val view = LayoutInflater.from(context)
            .inflate(R.layout.fragment_user_service_status_adapter, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {


        return list.size


    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {


//
        val bid=list.get(position).id

        val personname = list.get(position).services.fullname

        holder.name.text = personname


        holder.otp.text = "OTP:" + list.get(position).job_id.job_otp


        holder.status.text = list.get(position).job_id.job_status

        Log.e("status", "" + list.get(position).job_id.job_status)

        if (list.get(position).job_id.job_status.equals("Pending")) {

            holder.status.setTextColor(ContextCompat.getColor(context, R.color.quantum_orange))
        } else if (list.get(position).job_id.job_status.equals("InProgress")) {

            holder.status.setTextColor(ContextCompat.getColor(context, R.color.purple_200))
        } else if (list.get(position).job_id.job_status.equals("Completed")) {

            holder.status.setTextColor(ContextCompat.getColor(context, R.color.green))
            holder.paynowbutton.visibility = View.VISIBLE

        } else if (list.get(position).job_id.job_status.equals("Cancelled")) {

            holder.status.setTextColor(ContextCompat.getColor(context, R.color.red))
        }

        holder.paynowbutton.setOnClickListener {

            val editor = sharedPreferences.edit()
            editor.putString("name", list.get(position).services.fullname)
            editor.putString("amount", list.get(position).bid_amount)
            editor.putInt("jobid", list.get(position).job_id.id)
            editor.putInt("bid", list.get(position).id)
            editor.commit()

            Log.e("name", "" + list.get(position).services.fullname)
            Log.e("amount", "" + list.get(position).bid_amount)
            Log.e("bid", "" +  list.get(position).id)

            var intent = Intent(context, Razorpay_Payment_activity::class.java)
            context.startActivity(intent)
        }
        val profname = list.get(position).services.category.map {

            val profname = it.name

            holder.profession.text = profname

        }

        val bidamount = list.get(position).bid_amount

        holder.bidcost.text = bidamount


        var profileimage = list.get(position).services.profile_pic

        Glide.with(context).load(profileimage)
            .apply(RequestOptions().fitCenter())
            .error(R.drawable.electrician)
            .into(holder.servicepersonprofile)
    }


    class MyViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {


        val servicepersonprofile = itemView!!.findViewById(R.id.servicepersonprofile) as ImageView
        val name = itemView!!.findViewById(R.id.name) as TextView
        val profession = itemView!!.findViewById(R.id.profession) as TextView
        val bidcost = itemView!!.findViewById(R.id.bidcost) as TextView
        val status = itemView!!.findViewById(R.id.status) as TextView
        val otp = itemView!!.findViewById(R.id.otptxt) as TextView
        val paynowbutton = itemView!!.findViewById(R.id.paynow) as TextView


    }
}


