package com.supernal.oy.user.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.supernal.oy.R
import com.supernal.oy.adapters.Category_adapter
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentUserHomeBinding
import com.supernal.oy.models.CategoryDefaultResponse
import com.supernal.oy.models.CategoryListResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserHomeFragment : Fragment() {
    private var _binding: FragmentUserHomeBinding? = null
    lateinit var categoryAdapter: Category_adapter
    lateinit var categoriesListResponse: List<CategoryListResponse>
    lateinit var sharedPreferences: SharedPreferences

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUserHomeBinding.inflate(inflater, container, false)
        sharedPreferences = requireContext().getSharedPreferences(
            "loginprefs", Context.MODE_PRIVATE
        )
        var address = sharedPreferences.getString("add", "")

        binding.serviceCategoryRecycleview.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayout.HORIZONTAL, false)
        val root: View = binding.root

//        binding.plumbingimg.setOnClickListener {
//            val navController =
//                Navigation.findNavController(context as Activity, R.id.nav_host_fragment_content_oy_user)
//            navController.navigate(R.id.nav_user_serviceBooking)
//        }

        binding.locText1.text=address
        binding.locText1.setOnClickListener {

            val navController = Navigation.findNavController(
                requireActivity(),
                R.id.nav_host_fragment_content_oy_user
            )
            navController.navigate(R.id.nav_user_location)
            

        }
        getcategorylist()
        return root
    }

    //on item click interface
    interface OnItemClickListener {
        fun onItemClicked(position: Int, view: View)
    }

    fun RecyclerView.addOnItemClickListener(onClickListener: OnItemClickListener) {
        this.addOnChildAttachStateChangeListener(object :
            RecyclerView.OnChildAttachStateChangeListener {
            override fun onChildViewDetachedFromWindow(view: View) {
            }


            override fun onChildViewAttachedToWindow(view: View) {
                view.setOnClickListener {
                    val holder = getChildViewHolder(view)
                    onClickListener.onItemClicked(holder.adapterPosition, view)
                }
            }
        })
    }

    private fun getcategorylist() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)


                val call = apiServices.getcategories()

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<CategoryDefaultResponse> {
                    override fun onResponse(
                        call: Call<CategoryDefaultResponse>,
                        response: Response<CategoryDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())



                            categoriesListResponse = response.body()?.data!!


                            categoryAdapter = Category_adapter(
                                requireActivity(),
                                categoriesListResponse
                            )

                            binding.serviceCategoryRecycleview.adapter =
                                categoryAdapter
                             categoryAdapter.notifyDataSetChanged()

                            binding.serviceCategoryRecycleview.addOnItemClickListener(object :
                                OnItemClickListener {
                                override fun onItemClicked(position: Int, view: View) {
                                    // Your logic

                                    val editor = sharedPreferences.edit()
//
                                    val category_id = categoryAdapter?.list?.get(position)!!.id
                                    val category_name = categoryAdapter?.list?.get(position)!!.name

                                    val catid = response.body()?.data!!.map {
                                        val id = it.id

                                        val editor = sharedPreferences.edit()
                                        editor.putInt("category_id", id)
                                        editor.putString("category_name", category_name)

                                        editor.commit()
                                    }
                                    Log.e("category_id", "" + catid)


                                    if (category_id != null) {
                                        editor.putInt("category_id", category_id)
                                    }
                                    editor.putInt("category_id", category_id)
                                    editor.commit()
//
                                    if ((categoryAdapter!!.list.get(position).id != 0)) {


//                                    val reservecardFragment = ReseveCard_Fragment()
//                                    val ft = fragmentManager
//                                        ?.beginTransaction()
//                                        ?.replace(R.id.content_fragment, reservecardFragment)
//                                        ?.commit()

                                        val navController = Navigation.findNavController(
                                            activity!!,
                                            R.id.nav_host_fragment_content_oy_user
                                        )
                                        navController.navigate(R.id.nav_user_serviceBooking)
                                    } else {

                                        Toast.makeText(
                                            context,
                                            "No users Available",
                                            Toast.LENGTH_LONG
                                        )
                                            .show()

                                    }


                                }

                            })


//


                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<CategoryDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}