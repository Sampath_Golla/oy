package com.supernal.oy.service.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.supernal.oy.R
import com.supernal.oy.adapters.AutoCompleteAdapter
import java.util.*

class Oy_ProviderLocation:AppCompatActivity() {
    lateinit var location_search: Button
    lateinit var address_text: AutoCompleteTextView
    var placesClient: PlacesClient? = null
    var adapter: AutoCompleteAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.oy_provider_location)
        location_search=findViewById(R.id.done_Btn)
        address_text = findViewById(R.id.address_text)

        try {


            // Setup Places Client
            if (!Places.isInitialized()) {
//                Places.initialize(applicationContext, "AIzaSyAl_W-16U6yJI1RLf0gZdDd_IHoXFNi6L0")
                Places.initialize(applicationContext, "AIzaSyCWhZSGbO0WHgdWu35i2WMopQKKs2o0xQs")
            }

            placesClient = Places.createClient(this)
            address_text.threshold = 1
            address_text.onItemClickListener = autocompleteClickListener
            adapter = AutoCompleteAdapter(this, placesClient!!)
            address_text.setAdapter(adapter)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        location_search.setOnClickListener {
            startActivity(Intent(this@Oy_ProviderLocation, Oy_Provider::class.java))
        }
    }

    private val autocompleteClickListener =
        AdapterView.OnItemClickListener { adapterView, view, i, l ->
            try {
                val item: AutocompletePrediction = adapter?.getItem(i)!!
                var placeID: String? = null
                if (item != null) {
                    placeID = item.placeId
                }
                val placeFields: List<Place.Field> = Arrays.asList(
                    Place.Field.ID,
                    Place.Field.NAME,
                    Place.Field.ADDRESS,
                    Place.Field.LAT_LNG
                )
                var request: FetchPlaceRequest? = null
                if (placeID != null) {
                    request = FetchPlaceRequest.builder(placeID, placeFields)
                        .build()
                }
                if (request != null) {
                    placesClient?.fetchPlace(request)
                        ?.addOnSuccessListener(OnSuccessListener<Any> { task ->
                            Log.e(
                                "", "" +
                                        task

                            )
                        })?.addOnFailureListener(
                            OnFailureListener { e ->
                                e.printStackTrace()
                                address_text.setText(e.message)
                            })
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

}
