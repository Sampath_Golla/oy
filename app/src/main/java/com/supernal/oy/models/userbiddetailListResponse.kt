package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class userbiddetailListResponse(

    @SerializedName("id") val id : Int,
    @SerializedName("job_id") val job_id : userbidjobidResponse,
    @SerializedName("bid_amount") val bid_amount : String,
    @SerializedName("services") val services : userbidservicesResponse

    )