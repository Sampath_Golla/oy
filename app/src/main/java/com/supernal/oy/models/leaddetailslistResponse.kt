package com.supernal.oy.models

import com.google.gson.annotations.SerializedName

data class leaddetailslistResponse(

    @SerializedName("id") val id : Int,
    @SerializedName("full_name") val full_name : String,
    @SerializedName("profile_pic") val profile_pic : String,
    @SerializedName("mobile") val mobile : String,
    @SerializedName("address") val address : String,
    @SerializedName("job_title") val job_title : String,
    @SerializedName("job_description") val job_description : String,
    @SerializedName("job_image") val job_image : String,
    @SerializedName("date_scheduled") val date_scheduled : String,
    @SerializedName("time_scheduled") val time_scheduled : String,
    @SerializedName("job_status") val job_status : String,
    @SerializedName("job_rating") val job_rating : String,
    @SerializedName("posted_on") val posted_on : String
)