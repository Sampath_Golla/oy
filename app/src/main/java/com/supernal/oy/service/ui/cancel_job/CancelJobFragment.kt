package com.supernal.oy.service.ui.cancel_job

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.supernal.oy.R
import com.supernal.oy.adapters.Reasons_adapter
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentCancelJobBinding
import com.supernal.oy.models.*
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class CancelJobFragment : Fragment() {

    private lateinit var canceljobviewModel: CancelJobViewModel
    private var _binding: FragmentCancelJobBinding? = null
    private val binding get() = _binding!!
    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var comment: String
    lateinit var reasonsListResponse: List<Cancel_ReasonResponse>
    lateinit var reasonadapter: Reasons_adapter
    lateinit var spinn: Spinner
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        canceljobviewModel =
            ViewModelProvider(this).get(CancelJobViewModel::class.java)
        _binding = FragmentCancelJobBinding.inflate(inflater, container, false)
        servicesharedPreferences = requireContext().getSharedPreferences(
            "serviceloginprefs", Context.MODE_PRIVATE
        )
        val root: View = binding.root
        binding.button.setOnClickListener {

            comment = binding.comment.text.toString().trim()

            canceljob()
        }
        spinn = root.findViewById(R.id.reason_spinner) as Spinner

        getjobinfo()
        getreasonlist()
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun getjobinfo() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = servicesharedPreferences.getString("servicetoken", "")
                var jid = servicesharedPreferences.getInt("jobid", 0)

                Log.e("API : ", "" + apiServices)


//                val call = apiServices.leadslist("Token $token")
//                val call =
//                    apiServices.jobindetailresponse(
//                        "Token $token",
//                        jid

                val call =
                    apiServices.jobindetailresponse(
                        "Token $token", jid
                    )

                Log.e("API Categories : ", "" + call)

                call.enqueue(object : Callback<MyJobsdetaildefaultResponse> {
                    override fun onResponse(
                        call: Call<MyJobsdetaildefaultResponse>,
                        response: Response<MyJobsdetaildefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())


                            binding.provider.text = response.body()!!.data.first_name
                            binding.postedTime.text =
                                " Posted on  " + response.body()!!.data.posted_on

                            var serviceimage = response.body()!!.data.job_image
                            var profilepic = response.body()!!.data.profile_pic

                            context?.let {
                                Glide.with(it).load(profilepic)
                                    .apply(RequestOptions().fitCenter())
                                    .error(R.drawable.electrician)
                                    .into(binding.customerImg)
                            }

                            context?.let {
                                Glide.with(it).load(serviceimage)
                                    .apply(RequestOptions().fitCenter())
                                    .error(R.drawable.electrician)
                                    .into(binding.workRelatedImg)
                            }

                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<MyJobsdetaildefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }

    private fun canceljob() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = servicesharedPreferences.getString("servicetoken", "")
                var jid = servicesharedPreferences.getInt("jobid", 0)

                Log.e("API : ", "" + apiServices)


                val call =
                    apiServices.canceljob(
                        "Token $token", jid, "", comment
                    )

                Log.e("API Categories : ", "" + call)

                call.enqueue(object : Callback<CancelDefaultResponse> {
                    override fun onResponse(
                        call: Call<CancelDefaultResponse>,
                        response: Response<CancelDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())

                            val navController = Navigation.findNavController(
                                activity!!,
                                R.id.nav_host_fragment_content_oy_provider
                            )
                            navController.navigate(R.id.nav_home)

                            Toast.makeText(activity, "Job Cancelled", Toast.LENGTH_LONG).show()

                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<CancelDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }

    private fun getreasonlist() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)


                Log.e("API : ", "" + apiServices)


                val call = apiServices.getreasonslist()

                Log.e("API Categories : ", "" + call)

                call.enqueue(object : Callback<Cancel_Reason_DefaultResponse> {
                    override fun onResponse(
                        call: Call<Cancel_Reason_DefaultResponse>,
                        response: Response<Cancel_Reason_DefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())

                            reasonsListResponse = response.body()!!.data

                            reasonadapter = Reasons_adapter(requireActivity(), reasonsListResponse)

                            binding.reasonSpinner.adapter=reasonadapter
                            reasonadapter.notifyDataSetChanged()





                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(
                        call: Call<Cancel_Reason_DefaultResponse>,
                        t: Throwable?
                    ) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }


}