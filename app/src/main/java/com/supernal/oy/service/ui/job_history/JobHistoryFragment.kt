package com.supernal.oy.service.ui.job_history

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.supernal.oy.adapters.My_History_adapter
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentJobHistoryBinding
import com.supernal.oy.models.*
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class JobHistoryFragment : Fragment(), AdapterView.OnItemSelectedListener {
    private lateinit var jobHistoryViewModel: JobHistoryViewModel
    private var _binding: FragmentJobHistoryBinding? = null
    private val binding get() = _binding!!
    lateinit var token: String
    lateinit var servicesharedPreferences: SharedPreferences
    lateinit var HistoryAdapter: My_History_adapter
    lateinit var jobsListResponse: List<HistoryListResponse>
    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        jobHistoryViewModel =
            ViewModelProvider(this).get(JobHistoryViewModel::class.java)
        _binding = FragmentJobHistoryBinding.inflate(inflater, container, false)
        servicesharedPreferences = requireContext().getSharedPreferences(
            "serviceloginprefs", Context.MODE_PRIVATE
        )

        val root: View = binding.root
        token = servicesharedPreferences.getString("servicetoken", "").toString()
        binding.myhistoryRecycleview.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayout.VERTICAL, false)
        getjobhistory()
        return root
    }

    private fun getjobhistory() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)

                Log.e("API : ", "" + apiServices)


                val call =
                    apiServices.getjobhistory(
                        "Token $token","")


                Log.e("API Categories : ", "" + call)
                Log.e("token : ", "" + token)


                call.enqueue(object : Callback<HistoryDefaultResponse> {
                    override fun onResponse(
                        call: Call<HistoryDefaultResponse>,
                        response: Response<HistoryDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())

                            jobsListResponse = response.body()?.data!!
                            if (jobsListResponse.isEmpty()) {
                                binding.myhistoryRecycleview.visibility = View.GONE
//                                binding.nodata.visibility = View.VISIBLE
                            } else {

                                HistoryAdapter = My_History_adapter(
                                    requireActivity(),
                                    jobsListResponse
                                )
                                binding.myhistoryRecycleview.adapter =
                                    HistoryAdapter
                                HistoryAdapter.notifyDataSetChanged()


                            }
//


                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(call: Call<HistoryDefaultResponse>, t: Throwable?) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        } catch (e2: NumberFormatException) {
            e2.printStackTrace()
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val positonInt = Integer.valueOf(position)
        Toast.makeText(context, "value is $positonInt", Toast.LENGTH_SHORT).show()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }
}