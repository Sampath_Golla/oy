package com.supernal.oy.user.ui.servicestatus

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.navigation.Navigation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.supernal.oy.R
import com.supernal.oy.api.Api
import com.supernal.oy.api.ApiClient
import com.supernal.oy.databinding.FragmentUserServicebookingBinding
import com.supernal.oy.models.CategoryServicePersonsDefaultResponse
import com.supernal.oy.models.CategoryServicePersonsListResponse
import com.supernal.oy.utils.NetWorkConection
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class UserServiceBookingFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMapClickListener {
    private var _binding: FragmentUserServicebookingBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private var myMaps: GoogleMap? = null
    private lateinit var startMarker: Marker
    lateinit var currentLocation: Location
    lateinit var geocoder: Geocoder
    var lng :Double = 0.0
    lateinit var servicepersonslist: List<CategoryServicePersonsListResponse>

    lateinit var sharedPreferences: SharedPreferences

    var lat :Double = 0.0
    var mindspace = LatLng(17.4416, 78.3796)
    var nanakramguda = LatLng(17.4169, 78.3424)
    var ikea = LatLng(17.4392, 78.3754)
    private lateinit var locationArray: ArrayList<LatLng>
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val permissionCode=101
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentUserServicebookingBinding.inflate(inflater, container, false)
        sharedPreferences = requireContext().getSharedPreferences(
            "loginprefs", Context.MODE_PRIVATE
        )
        val root: View = binding.root
        binding.booknowBtn.setOnClickListener {
            val navController =
                Navigation.findNavController(context as Activity, R.id.nav_host_fragment_content_oy_user)
            navController.navigate(R.id.nav_user_uploadService)
        }
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context as Activity)
        fetchLocation()

        getservicepeoplelist()
        return root
    }

    private fun fetchLocation() {
        if(ActivityCompat.checkSelfPermission(
                context as Activity,android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context as Activity, android.Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions( context as Activity, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),permissionCode)
            return
        }
        val task=fusedLocationProviderClient.lastLocation
        task.addOnSuccessListener { location->
            if(location!=null) {
                currentLocation = location
                Toast.makeText(
                    context,
                    currentLocation.latitude.toString() + "" + currentLocation.longitude.toString(),
                    Toast.LENGTH_SHORT
                ).show()
                val supportMapFragment =
                    childFragmentManager.findFragmentById(R.id.currentLocation) as SupportMapFragment?
                supportMapFragment?.getMapAsync(this)
                locationArray = ArrayList<LatLng>()
                locationArray.add(mindspace)
                locationArray.add(nanakramguda)
                locationArray.add(ikea)
            }}}



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onMapReady(googleMaps: GoogleMap) {
        lat =currentLocation.latitude
        lng =currentLocation.longitude
        val latlng= LatLng(lat,lng)
        googleMaps.setMapType(GoogleMap.MAP_TYPE_NORMAL)
        val markerOptions= MarkerOptions().position(latlng).draggable(true)
        googleMaps.setOnMapClickListener (this)
        googleMaps.animateCamera(CameraUpdateFactory.newLatLng(latlng))
        googleMaps.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng,12.0f))
        for (i in 0 until locationArray.size) {
            createMarker(locationArray.get(i).latitude, locationArray.get(i).longitude);
            // below line is use to add marker to each location of our array list.
            myMaps?.addMarker(MarkerOptions().position(locationArray.get(i)).title("Marker"))

            // below lin is use to zoom our camera on map.
            myMaps?.animateCamera(CameraUpdateFactory.zoomTo(14.0f))

            // below line is use to move our camera to the specific location.
            myMaps?.moveCamera(CameraUpdateFactory.newLatLng(locationArray.get(i)))
            startMarker=googleMaps.addMarker(MarkerOptions().position(locationArray.get(i)).title("Marker"))

        }
    }


    protected fun createMarker(
        latitude: Double,
        longitude: Double,
    ): Marker? {
        return myMaps?.addMarker(
            MarkerOptions()
                .position(LatLng(latitude, longitude))

        )
    }
    override fun onMapClick(p0: LatLng) {
    }



    private fun getservicepeoplelist() {


        try {

            if (activity?.let { NetWorkConection.isNEtworkConnected(it) }!!) {


                //Set the Adapter to the RecyclerView//

                var apiServices = ApiClient.client.create(Api::class.java)
                var token = sharedPreferences.getString("token", "")
                var catid = sharedPreferences.getInt("category_id", 0)

                Log.e("API : ", "" + apiServices)


                val call = apiServices.Categoryservicepersonslist("Token $token", catid)

                Log.e("API Categories : ", "" + call)


                call.enqueue(object : Callback<CategoryServicePersonsDefaultResponse> {
                    override fun onResponse(
                        call: Call<CategoryServicePersonsDefaultResponse>,
                        response: Response<CategoryServicePersonsDefaultResponse>
                    ) {

                        if (response.isSuccessful) {
                            Log.e("user response", "" + response.body()?.toString())

                            servicepersonslist = response.body()?.data!!


                            val servloc=response.body()!!.data.map {

                                val lat=it.latitude
                                val lng=it.longitude

                            }



                        } else {
                            Toast.makeText(activity, "No Data", Toast.LENGTH_LONG).show()


                        }
                    }

                    override fun onFailure(
                        call: Call<CategoryServicePersonsDefaultResponse>,
                        t: Throwable?
                    ) {
                        Log.e("response", t.toString())
                    }
                })


            } else {


                Toast.makeText(activity, "Please Check your internet", Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(activity, "" + e.message, Toast.LENGTH_LONG).show()

        } catch (e1: JSONException) {
            e1.printStackTrace()
        }


    }

}
